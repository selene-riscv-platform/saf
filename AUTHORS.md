Authors
=======

* __[Charles-Alexis Lefebvre](https://github.com/xcharly)__

    Project Lead, Developer

    Ikerlan Technology Research Center, Basque Research and technology Alliance (BRTA), Arrasate-Mondragón (Gipuzkoa, Spain)

* __[Michael Sandoval](https://github.com/misan128)__

    Developer

    Ikerlan Technology Research Center, Basque Research and technology Alliance (BRTA), Arrasate-Mondragón (Gipuzkoa, Spain)
    
* __[Markel Sainz](https://github.com/mjonian93)__

    Developer

    Ikerlan Technology Research Center, Basque Research and technology Alliance (BRTA), Arrasate-Mondragón (Gipuzkoa, Spain)



