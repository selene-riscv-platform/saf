/* TODO: Include required standard libs */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <saf/utils.h>
#include <saf/saf.h>
#include "cJSON.h"

/***************************************************************************//**
 * @addtogroup UTILS
 * @{
 ******************************************************************************/

#ifdef _DEBUG
#define EVAL_JSON_OBJECT(json_obj)                      \
  do{                                                   \
  if ((json_obj) == NULL)                               \
  {                                                     \
    const char *error_ptr = cJSON_GetErrorPtr();        \
    if (error_ptr != NULL)                              \
    {                                                   \
      /* TODO: Define error feedback/prompt */          \
      fprintf(stderr, "JSON Error: %s\n", error_ptr);   \
    }                                                   \
    goto end;                                           \
  }                                                     \
} while(0)
#else
#define EVAL_JSON_OBJECT(json_obj)                      \
do{                                                     \
  if ((json_obj) == NULL)                               \
  { goto end; }                                         \
} while(0)
#endif


#define EVAL_NULL(ptr)                                  \
do{                                                     \
  if ((ptr) == NULL)                                    \
  { goto end;}                                          \
} while(0)


#define EVAL_FALSE(bool)                                \
do{                                                     \
  if ((bool) == SAF_FALSE)                              \
  { goto end;}                                          \
} while(0)

#define EVAL_STRING(json_obj)                         \
do{                                                     \
  if (!cJSON_IsString(json_obj))                      \
  { goto end;}                                          \
} while(0)

#define EVAL_NUMBER(json_obj)                         \
do{                                                     \
  if (!cJSON_IsNumber(json_obj))                      \
  { goto end;}                                          \
} while(0)

#define EVAL_ARRAY(json_obj)                          \
do{                                                     \
  if (!cJSON_IsArray(json_obj))                       \
  { goto end;}                                          \
} while(0)

#define WHICH_JSON(json_obj)       \
do {  \
  if (cJSON_IsInvalid(p_s_kernel))  \
  {  \
    fprintf(stderr, "cJSON_IsInvalid\n\r");  \
  }  \
  else if (cJSON_IsFalse(p_s_kernel))  \
  {  \
    fprintf(stderr, "cJSON_IsFalse\n\r");  \
  }  \
  else if (cJSON_IsTrue(p_s_kernel))  \
  {  \
    fprintf(stderr, "cJSON_IsTrue\n\r");  \
  }  \
  else if (cJSON_IsBool(p_s_kernel))  \
  {  \
    fprintf(stderr, "cJSON_IsBool\n\r");  \
  }  \
  else if (cJSON_IsNull(p_s_kernel))  \
  {  \
    fprintf(stderr, "cJSON_IsNull\n\r");  \
  }  \
  else if (cJSON_IsNumber(p_s_kernel))  \
  {  \
    fprintf(stderr, "cJSON_IsNumber\n\r");  \
  }  \
  else if (cJSON_IsString(p_s_kernel))  \
  {  \
    fprintf(stderr, "cJSON_IsString\n\r");  \
  }  \
  else if (cJSON_IsArray(p_s_kernel))  \
  {  \
    fprintf(stderr, "cJSON_IsArray\n\r");  \
  }  \
  else if (cJSON_IsObject(p_s_kernel))  \
  {  \
    fprintf(stderr, "cJSON_IsObject\n\r");  \
  }  \
  else if (cJSON_IsRaw(p_s_kernel))  \
  {  \
    fprintf(stderr, "cJSON_IsRaw\n\r");  \
  }  \
} while(0)


/* JSON Parsing Tags */
const char kernels_tag[] = "kernels";
const char name_tag[] = "name";
const char offset_tag[] = "offset";
const char size_tag[] = "size";
const char type_tag[] = "type";
const char data_type_tag[] = "data_type";
const char pos_tag[] = "pos";
const char base_addr_tag[] = "base_address";
const char ports_tag[] = "ports";
const char ctrl_tag[] = "control";
const char bits_tag[] = "bits";
const char start_tag[] = "ap_start";
const char done_tag[] = "ap_done";
const char idle_tag[] = "ap_idle";
const char ready_tag[] = "ap_ready";
const char restart_tag[] = "ap_restart";
const char continue_tag[] = "ap_continue";
const char signals_tag[] = "signals";

/* Kernel signal types */
const char kernel_signal_type_buffer_string[] = "buffer"; 
const char kernel_signal_type_value_string[] = "value"; 

const char kernel_signal_data_type_uint32_string[] = "uint32"; 
const char kernel_signal_data_type_uint64_string[] = "uint64";
const char kernel_signal_data_type_float_string[] = "float";

/**
 * @brief  Parses JSON config file and stores data into handling structures.
 * 
 * @param[in]   _p_ac_config_json  JSON input stream
 * @param[in]   _p_ac_kernel_name  Name of the kernel to look for in the stream
 * @param[out]  _p_s_kernel_config Structure to store data in.
 * 
 * @return                    SAF_TRUE on Success.
 *                            SAF_FALSE otherwise.
 */
SAF_BOOL Parse_JSON_Config_File(const char * const _p_ac_config_json,
                                const char * _p_ac_kernel_name,
                                s_Kernel_Config_t *_p_s_kernel_config)
{

  SAF_BOOL b_status = SAF_FALSE;
  SAF_BOOL b_kernel_found = SAF_FALSE;

  int i_signals = 0;
  int i_signal_cnt = 0;


  cJSON *p_s_config_json;

  const cJSON *p_s_kernels = NULL;
  const cJSON *p_s_kernel = NULL;
  const cJSON *p_s_kernel_name = NULL;
  const cJSON *p_s_kernel_addr = NULL;
  const cJSON *p_s_kernel_ports = NULL;
  const cJSON *p_s_kernel_ctrl = NULL;
  const cJSON *p_s_kernel_bits = NULL;
  const cJSON *p_s_kernel_ctrl_attr = NULL;
  const cJSON *p_s_kernel_signals = NULL;
  const cJSON *p_s_kernel_signal = NULL;
  const cJSON *p_s_kernel_signal_attr = NULL;

  /* Eval for params */
  if (!_p_ac_config_json || !_p_ac_kernel_name || !_p_s_kernel_config)
    { return SAF_FALSE; }

  DEBUG_LOG_1("Parsing Kernel Config File\n");
  DEBUG_LOG_1(" - Searching Kernel: %s\n", _p_ac_kernel_name);

  /* Set kernel config memory to 0 */
  memset(_p_s_kernel_config, 0, sizeof(s_Kernel_Config_t));

  /* Create JSON Parser Object */
  p_s_config_json = cJSON_Parse(_p_ac_config_json);


  /* Eval success parsing */
  EVAL_JSON_OBJECT(p_s_config_json);


  /* Get Kernel Array from JSON  */
  p_s_kernels = cJSON_GetObjectItemCaseSensitive(p_s_config_json, kernels_tag);
  EVAL_JSON_OBJECT(p_s_kernels);
  EVAL_ARRAY(p_s_kernels);

  /* Get count of signal objects */
  i_signals = cJSON_GetArraySize(p_s_kernels);
  if (i_signals == 0) {
    goto end;
  }

  DEBUG_LOG_1(" - Found Kernel Configs: %d\n\n", i_signals);
  i_signal_cnt = 0;


  /* Go through kernel objects in kernels tag */
  cJSON_ArrayForEach(p_s_kernel, p_s_kernels)
  {
    EVAL_JSON_OBJECT(p_s_kernel);

    ///////////////////////
    // PARSE KERNEL NAME //
    ///////////////////////

    p_s_kernel_name = cJSON_GetObjectItemCaseSensitive(p_s_kernel, name_tag);
    EVAL_JSON_OBJECT(p_s_kernel_name);
    EVAL_STRING(p_s_kernel_name);

    DEBUG_LOG_2("Kernel Config [%d]: %s\n\n", i_signal_cnt, p_s_kernel_name->valuestring);
    i_signal_cnt++;

    /* Eval for kernel available by name comparison */
    if (strcmp(p_s_kernel_name->valuestring, _p_ac_kernel_name) == 0) {

      b_kernel_found = SAF_TRUE;
      DEBUG_LOG_1("Found Kernel Config: %s\n", p_s_kernel_name->valuestring);

      /* Allocate memory for kernel name */
      _p_s_kernel_config->name = (char *)calloc(1, strlen(p_s_kernel_name->valuestring));
      EVAL_NULL(_p_s_kernel_config->name);

      strcpy(_p_s_kernel_config->name, p_s_kernel_name->valuestring);


      //////////////////////////////
      //PARSE KERNEL BASE ADDRESS //
      //////////////////////////////
      p_s_kernel_addr = cJSON_GetObjectItemCaseSensitive(p_s_kernel, base_addr_tag);
      EVAL_JSON_OBJECT(p_s_kernel_addr);
      EVAL_STRING(p_s_kernel_addr);

      /* Perform string hex parsing and eval success */
      EVAL_FALSE(Str_2_HEX(p_s_kernel_addr->valuestring, strlen(p_s_kernel_addr->valuestring), &(_p_s_kernel_config->base_addr)));
      DEBUG_LOG_1("Kernel Base Address: 0x%X\n",_p_s_kernel_config->base_addr);


      ////////////////////////
      // PARSE KERNEL PORTS //
      ////////////////////////
      DEBUG_LOG_1("Parsing Kernel Ports\n");
      p_s_kernel_ports = cJSON_GetObjectItemCaseSensitive(p_s_kernel, ports_tag);
      EVAL_JSON_OBJECT(p_s_kernel_ports);

      //////////////////////////////////////
      // PARSE KERNEL CONTROL PORT CONFIG //
      //////////////////////////////////////
      DEBUG_LOG_1("Parsing Kernel Control Register\n");
      p_s_kernel_ctrl = cJSON_GetObjectItemCaseSensitive(p_s_kernel_ports, ctrl_tag);
      EVAL_JSON_OBJECT(p_s_kernel_ctrl);


      /* Get control reg offset */
      p_s_kernel_ctrl_attr = cJSON_GetObjectItemCaseSensitive(p_s_kernel_ctrl, offset_tag);
      EVAL_JSON_OBJECT(p_s_kernel_ctrl_attr);
      EVAL_STRING(p_s_kernel_ctrl_attr);

      /* Perform string hex parsing and eval success */
      EVAL_FALSE(Str_2_HEX(p_s_kernel_ctrl_attr->valuestring, strlen(p_s_kernel_ctrl_attr->valuestring), ((uint32_t *)&(_p_s_kernel_config->control_reg.offset))));

      /* Get control reg bit assign */
      p_s_kernel_bits = cJSON_GetObjectItemCaseSensitive(p_s_kernel_ctrl, bits_tag);
      EVAL_JSON_OBJECT(p_s_kernel_bits);


      ///////////////////////////////
      // PARSE KERNEL CONTROL BITS //
      ///////////////////////////////
      DEBUG_LOG_1("Parsing Kernel Control Register Bits\n");
      /* Start */
      p_s_kernel_ctrl_attr = cJSON_GetObjectItemCaseSensitive(p_s_kernel_bits, start_tag);
      EVAL_JSON_OBJECT(p_s_kernel_ctrl_attr);
      EVAL_STRING(p_s_kernel_ctrl_attr);

      /* Perform string hex parsing and eval success */
      EVAL_FALSE(Str_2_HEX(p_s_kernel_ctrl_attr->valuestring, strlen(p_s_kernel_ctrl_attr->valuestring), ((uint32_t *)&(_p_s_kernel_config->control_reg.bit_pos.ap_start))));

      /* done */
      p_s_kernel_ctrl_attr = cJSON_GetObjectItemCaseSensitive(p_s_kernel_bits, done_tag);
      EVAL_JSON_OBJECT(p_s_kernel_ctrl_attr);
      EVAL_STRING(p_s_kernel_ctrl_attr);

      /* Perform string hex parsing and eval success */
      EVAL_FALSE(Str_2_HEX(p_s_kernel_ctrl_attr->valuestring, strlen(p_s_kernel_ctrl_attr->valuestring), ((uint32_t *)&(_p_s_kernel_config->control_reg.bit_pos.ap_done))));

      /* Idle */
      p_s_kernel_ctrl_attr = cJSON_GetObjectItemCaseSensitive(p_s_kernel_bits, idle_tag);
      EVAL_JSON_OBJECT(p_s_kernel_ctrl_attr);
      EVAL_STRING(p_s_kernel_ctrl_attr);

      /* Perform string hex parsing and eval success */
      EVAL_FALSE(Str_2_HEX(p_s_kernel_ctrl_attr->valuestring, strlen(p_s_kernel_ctrl_attr->valuestring), ((uint32_t *)&(_p_s_kernel_config->control_reg.bit_pos.ap_idle))));

      /* Ready */
      p_s_kernel_ctrl_attr = cJSON_GetObjectItemCaseSensitive(p_s_kernel_bits, ready_tag);
      EVAL_JSON_OBJECT(p_s_kernel_ctrl_attr);
      EVAL_STRING(p_s_kernel_ctrl_attr);

      /* Perform string hex parsing and eval success */
      EVAL_FALSE(Str_2_HEX(p_s_kernel_ctrl_attr->valuestring, strlen(p_s_kernel_ctrl_attr->valuestring), ((uint32_t *)&(_p_s_kernel_config->control_reg.bit_pos.ap_ready))));

      /* Restart */
      p_s_kernel_ctrl_attr = cJSON_GetObjectItemCaseSensitive(p_s_kernel_bits, restart_tag);
      EVAL_JSON_OBJECT(p_s_kernel_ctrl_attr);
      EVAL_STRING(p_s_kernel_ctrl_attr);

      /* Perform string hex parsing and eval success */
      EVAL_FALSE(Str_2_HEX(p_s_kernel_ctrl_attr->valuestring, strlen(p_s_kernel_ctrl_attr->valuestring), ((uint32_t *)&(_p_s_kernel_config->control_reg.bit_pos.ap_restart))));

      /* Continue */
      p_s_kernel_ctrl_attr = cJSON_GetObjectItemCaseSensitive(p_s_kernel_bits, continue_tag);

      /* NOTE: Dynamic signal ap_continue */
      if (p_s_kernel_ctrl_attr != NULL) {
        EVAL_JSON_OBJECT(p_s_kernel_ctrl_attr);
        EVAL_STRING(p_s_kernel_ctrl_attr);

        /* Perform string hex parsing and eval success */
        EVAL_FALSE(Str_2_HEX(p_s_kernel_ctrl_attr->valuestring, strlen(p_s_kernel_ctrl_attr->valuestring), ((uint32_t *)&(_p_s_kernel_config->control_reg.bit_pos.ap_continue))));
      }

      //////////////////////////
      // PARSE KERNEL SIGNALS //
      //////////////////////////
      DEBUG_LOG_1("Parsing Kernel Signals\n");
      p_s_kernel_signals = cJSON_GetObjectItemCaseSensitive(p_s_kernel_ports, signals_tag);
      EVAL_JSON_OBJECT(p_s_kernel_signals);
      EVAL_ARRAY(p_s_kernel_signals);

      /* Get count of signal objects */
      i_signals = cJSON_GetArraySize(p_s_kernel_signals);
      if (i_signals == 0) {
        goto end;
      }

      DEBUG_LOG_1(" - Kernel Signals Found: %d\n\n", i_signals);

      /* Store signal size */
      _p_s_kernel_config->signals_size = i_signals;

      /* Allocate i_signals structure memory */
      _p_s_kernel_config->signals = (s_Kernel_Config_Signal_t *)calloc(1, i_signals * (sizeof(s_Kernel_Config_Signal_t)));

      i_signal_cnt = 0;
      cJSON_ArrayForEach(p_s_kernel_signal, p_s_kernel_signals)
      {
        EVAL_JSON_OBJECT(p_s_kernel_signal);
        DEBUG_LOG_1("Parsing Kernel Signal[%d]\n", i_signal_cnt);

        //////////////////////////////
        // PARSE KERNEL SIGNAL NAME //
        //////////////////////////////
        p_s_kernel_signal_attr = cJSON_GetObjectItemCaseSensitive(p_s_kernel_signal, name_tag);
        EVAL_JSON_OBJECT(p_s_kernel_signal_attr);
        EVAL_STRING(p_s_kernel_signal_attr);
        
        /* Allocate memory for kernel signal name */
        _p_s_kernel_config->signals[i_signal_cnt].name = (char *)calloc(1, strlen(p_s_kernel_signal_attr->valuestring));
        EVAL_NULL(_p_s_kernel_config->signals[i_signal_cnt].name);


        strcpy(_p_s_kernel_config->signals[i_signal_cnt].name, p_s_kernel_signal_attr->valuestring);
        DEBUG_LOG_1(" - Name: %s\n", _p_s_kernel_config->signals[i_signal_cnt].name);

        /*Get signal data_type*/
        p_s_kernel_signal_attr = cJSON_GetObjectItemCaseSensitive(p_s_kernel_signal, data_type_tag);
        EVAL_JSON_OBJECT(p_s_kernel_signal_attr);
        EVAL_STRING(p_s_kernel_signal_attr);

        if (strcmp(p_s_kernel_signal_attr->valuestring, kernel_signal_data_type_uint32_string) == 0) {
          _p_s_kernel_config->signals[i_signal_cnt].data_type = E_KERNEL_SIGNAL_DATA_TYPE_UINT32;
        } else if (strcmp(p_s_kernel_signal_attr->valuestring, kernel_signal_data_type_uint64_string) == 0) {
          _p_s_kernel_config->signals[i_signal_cnt].data_type = E_KERNEL_SIGNAL_DATA_TYPE_UINT64;
        } else if (strcmp(p_s_kernel_signal_attr->valuestring, kernel_signal_data_type_float_string) == 0) {
          _p_s_kernel_config->signals[i_signal_cnt].data_type = E_KERNEL_SIGNAL_DATA_TYPE_FLOAT;
        } else {
          /* Error */
          goto end;
        }

        /* Get signal offset */
        p_s_kernel_signal_attr = cJSON_GetObjectItemCaseSensitive(p_s_kernel_signal, offset_tag);
        EVAL_JSON_OBJECT(p_s_kernel_signal_attr);
        EVAL_STRING(p_s_kernel_signal_attr);

        /* Perform string hex parsing and eval success */
        EVAL_FALSE(Str_2_HEX(p_s_kernel_signal_attr->valuestring, strlen(p_s_kernel_signal_attr->valuestring), 
          ((uint32_t *)&(_p_s_kernel_config->signals[i_signal_cnt].offset))));
        /* Use strtol to convert hex string to int */
        //_p_s_kernel_config->signals[i_signal_cnt].offset = (uint32_t)strtol(p_s_kernel_signal_attr->valuestring, NULL, 0);
        DEBUG_LOG_2(" - Offset: %d\n", _p_s_kernel_config->signals[i_signal_cnt].offset);

        /* Get signal bit size */
        p_s_kernel_signal_attr = cJSON_GetObjectItemCaseSensitive(p_s_kernel_signal, size_tag);
        EVAL_JSON_OBJECT(p_s_kernel_signal_attr);
        EVAL_NUMBER(p_s_kernel_signal_attr);

        _p_s_kernel_config->signals[i_signal_cnt].size_bits = p_s_kernel_signal_attr->valuedouble;
        DEBUG_LOG_2(" - Data size: %d\n", _p_s_kernel_config->signals[i_signal_cnt].size_bits);


        /* Get register value position */
        p_s_kernel_signal_attr = cJSON_GetObjectItemCaseSensitive(p_s_kernel_signal, pos_tag);
        EVAL_JSON_OBJECT(p_s_kernel_signal_attr);
        EVAL_NUMBER(p_s_kernel_signal_attr);

        _p_s_kernel_config->signals[i_signal_cnt].pos = p_s_kernel_signal_attr->valuedouble;
        DEBUG_LOG_2(" - Register position start: %d\n", _p_s_kernel_config->signals[i_signal_cnt].pos);

        /* Get signal value type */
        p_s_kernel_signal_attr = cJSON_GetObjectItemCaseSensitive(p_s_kernel_signal, type_tag);
        EVAL_JSON_OBJECT(p_s_kernel_signal_attr);
        EVAL_STRING(p_s_kernel_signal_attr);

        if (strcmp(p_s_kernel_signal_attr->valuestring, kernel_signal_type_buffer_string) == 0) {
          _p_s_kernel_config->signals[i_signal_cnt].type = E_KERNEL_SIGNAL_BUFFER;
          DEBUG_LOG_1(" - Type: Buffer\n");
        } else if (strcmp(p_s_kernel_signal_attr->valuestring, kernel_signal_type_value_string) == 0) {
          _p_s_kernel_config->signals[i_signal_cnt].type = E_KERNEL_SIGNAL_VALUE;
          DEBUG_LOG_1(" - Type: Parameter value\n");
        } else {
          /* Error */
          DEBUG_LOG_1("ERROR: Unknown signal type\n");
          goto end;
        }

        i_signal_cnt++;
      }

      /* Successful parsing */
      DEBUG_LOG_1("Parsing Kernel Config OK\n");
      b_status = SAF_TRUE;
      goto end;
    }

  }
  
  
  end:

    /* Clean Exit */
    if (b_status != SAF_TRUE) {

      if (b_kernel_found != SAF_TRUE) {
        DEBUG_LOG_1("Kernel %s not found\n", _p_ac_kernel_name);
      }

      DEBUG_LOG_1("Parsing Kernel Config Error\n");
      
      /* Clean kernel config allocated memory */
      Free_Kernel_Config(_p_s_kernel_config);

      /* Clean kernel config structure memory */
      memset(_p_s_kernel_config, 0, sizeof(s_Kernel_Config_t));
    } else {
      /* update kernel max offset */
      _p_s_kernel_config->offset = _p_s_kernel_config->signals[_p_s_kernel_config->signals_size].offset;
    }

    /* Free Allocated Memory */
    cJSON_Delete(p_s_config_json); 

    //printf("After eorror in parsing\n");

    return b_status; /* Return Status */
}

/**
 * @brief  Frees memory allocated to store JSON config file data.
 * 
 * @param[inout]  _p_s_kernel_config Structure of stored data.
 * 
 * @return                    SAF_TRUE on Success.
 *                            SAF_FALSE otherwise.
 */
SAF_BOOL Free_Kernel_Config(s_Kernel_Config_t *_p_s_kernel_config) {

  uint8_t i_sig;

  /* Eval params */
  if (!_p_s_kernel_config)
    { return SAF_FALSE; }

  /* Free Kernel id */
  free(_p_s_kernel_config->name);

  /* Eval if signal structures were allocated */
  if (_p_s_kernel_config->signals){

    for (i_sig = 0; i_sig < _p_s_kernel_config->signals_size; i_sig++) {
      /* Free kernel's signal id malloc */
      free(_p_s_kernel_config->signals[i_sig].name);
    }

    /* Free k_signals malloc */
    free(_p_s_kernel_config->signals);
  }

  return SAF_TRUE;
}


/**
 * @brief  A function to translate from string hex representations to uint32_t values.
 * 
 * @param[in]   _p_c_hex          Input string hex.
 * @param[in]   num_bytes         Byte length in string.
 * @param[out]  _p_u32_dest_value Output number value.
 * 
 * @return                   SAF_TRUE on Success.
 *                           SAF_FALSE otherwise.
 */
SAF_BOOL Str_2_HEX(char *_p_c_hex, uint8_t num_bytes, uint32_t *_p_u32_dest_value) {

  char ac_hex_buf[8];
  // char pc_end_hex_ptr*; /* Required by */

  /* NOTE: num bytes can't be lower than 3 if using "0x" format */
  /* numbytes 10 = "0x" + 32 bits value (8 Bytes) */
  if (!_p_c_hex || num_bytes < 3 || num_bytes > 10 || !_p_u32_dest_value)
    { return SAF_FALSE; }

  /* Eval for hexadecil format syntax */
  if (_p_c_hex[0] != '0' || ((_p_c_hex[1] != 'x') && (_p_c_hex[1] != 'X')))
    { return SAF_FALSE; }

  //memcpy(ac_hex_buf, &_p_c_hex[2], num_bytes-2);

  //*_p_u32_dest_value = strtol(ac_hex_buf, NULL, 16);
  *_p_u32_dest_value = strtol(_p_c_hex, NULL, 0);

  /* TODO: strtol returns zero on error but some values could be zero.
           How to eval errors? */

  return SAF_TRUE;
}


/**
 * @brief Reads the JSON config file and stores the content into a char stream.
 * 
 * @param[in]  _p_c_config_file_path Config file path.
 * @param[out]  _p_c_config_json      Output JSON stream
 * 
 * @return                       SAF_TRUE on Success.
 *                               SAF_FALSE otherwise.
 */
SAF_BOOL Json_To_Buffer(const char * _p_c_config_file_path, char ** _p_c_config_json) {
  FILE *fp;
  long lSize;

  fp = fopen (_p_c_config_file_path , "rb" );
  if( !fp ) perror(_p_c_config_file_path),exit(1);

  fseek( fp , 0L , SEEK_END);
  lSize = ftell( fp );
  rewind( fp );

  /* allocate memory for entire content */
  *_p_c_config_json = (char *)calloc( 1, lSize+1 );
  if( !*_p_c_config_json ) {
    fclose(fp);
    fputs("memory alloc fails",stderr);
    return SAF_FALSE;
  } 

  /* copy the file into the buffer */
  if( 1!=fread( *_p_c_config_json , lSize, 1 , fp) ) {
    fclose(fp);
    free(*_p_c_config_json);
    fputs("entire read fails",stderr);
    return SAF_FALSE;
  }

  fclose(fp);

  return SAF_TRUE;
}


/**
 * @brief Modify the size of memory segment for system page size alignment
 * 
 * @param[in]  size Intended allocation size
 * 
 * @return     Page aligned size
 */
unsigned long Align_For_Page_Size(unsigned long size)
{
    if (size % PAGE_SIZE) {
      return ((size/PAGE_SIZE) * PAGE_SIZE + PAGE_SIZE);
    }

    return size;
}

/** @} (end addtogroup UTILS) *******************************************/
