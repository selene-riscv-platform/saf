#ifndef _IRD_IOCTL_H
#define _IRD_IOCTL_H

/*****************************************************************************
**  Header files
******************************************************************************/
#include <linux/ioctl.h>

#ifndef __KERNEL__
    #include <stdint.h>
#else
    #include <linux/types.h>
#endif

/*****************************************************************************
**  Macro definition
******************************************************************************/

/*! Device file name */
#define IRD_DEV_FILENAME "ird"

/*! Major device number */
#define MAJOR_NUM 10

/*! Request interruption handler */
#define IRD_IOCTL_WAIT_INTERRUPTION _IOWR(MAJOR_NUM, 0, struct ird_io *)

/*****************************************************************************
**  Object or Struct Declaration
******************************************************************************/

/*! Interruption object structure 
 * @status: Status of the interruption
 * @myHZ: Jiffies per second in CPU
 */

struct ird_io {
    int status;
    int myHZ;
};

#endif /* _IRD_IOCTL_H */