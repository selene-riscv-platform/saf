CROSS_CC = ${CROSS_COMPILE}gcc
CROSS_AR = ${CROSS_COMPILE}ar

LIBNAME = libSAF

STATIC_LIBRARY_NAME = $(LIBNAME).a
DYNAMIC_LIBRARY_NAME = $(LIBNAME).so

DBG_LVL ?= 0

#Get the git commit version
COMMIT := $(shell git describe --always --dirty --match 'NOT A TAG' --abbrev=40 )
COMMIT := $(COMMIT)   
COMMIT := '"$(COMMIT) $(shell git describe --all --long)"'
$(info $$COMMIT is [${COMMIT}])

CFLAGS= -fPIC -O0 -g -Wall -Wextra -c -DSAF_DEBUG=$(DBG_LVL) -DCOMMIT=$(COMMIT)
LFLAGS= -lpthread

SRC = $(wildcard ${CURDIR}/*.c)
OBJ = $(patsubst %.c,%.o, $(SRC))
INC = -I${CURDIR}/include

# Enumerating of every *.c as *.o and using that as dependency.	
# filter list of .c files in a directory.
#
# $(OUT_FILE_NAME): $(patsubst %.c,$(OBJ_DIR)/%.o,$(wildcard $(FILES)))

.PHONY: all clean

default: help

all: $(STATIC_LIBRARY_NAME) $(DYNAMIC_LIBRARY_NAME)

# Enumerating of every *.c as *.o and using that as dependency
$(STATIC_LIBRARY_NAME): $(OBJ)
	$(CROSS_AR) -r -o $@ $^ 

$(DYNAMIC_LIBRARY_NAME): $(OBJ)
	$(CROSS_CC) -shared -o $@ $^

#Compiling every *.c to *.o
%.o: %.c
	$(CROSS_CC) $(LFLAGS) -c $(CFLAGS) $(INC) -o $@  $<
	
clean:
	rm -f *.o $(STATIC_LIBRARY_NAME) $(DYNAMIC_LIBRARY_NAME)

help:
	@echo ""
	@echo "\tUsage: [variable_overrides] make [lib_type]"
	@echo ""
	@echo "Define CROSS_COMPILE before compiling"
	@echo ""
	@echo "make $(STATIC_LIBRARY_NAME) - Make the static library"
	@echo "make $(DYNAMIC_LIBRARY_NAME) - Make the shared library"
	@echo "make all - Make both libraries"
	@echo "make clean - Clean all"
