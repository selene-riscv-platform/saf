#ifndef UTILS_H
#define UTILS_H

#include "data_types.h"
#include "kernel_spec.h"

/***************************************************************************//**
 * @addtogroup UTILS
 * @brief
 *   Utility functions
 * @{
 ******************************************************************************/

#define PAGE_SIZE 4096

/**
 * @brief  Parses JSON config file and stores data into handling structures.
 * 
 * @param[in]   _p_ac_config_json  JSON input stream
 * @param[in]   _p_ac_kernel_name  Name of the kernel to look for in the stream
 * @param[out]  _p_s_kernel_config Structure to store data in.
 * 
 * @return                    SAF_TRUE on Success.
 *                            SAF_FALSE otherwise.
 */
SAF_BOOL Parse_JSON_Config_File(const char * const _p_ac_config_json,
                                const char * _p_ac_kernel_name,
                                s_Kernel_Config_t *_p_s_kernel_config);

/**
 * @brief  Frees memory allocated to store JSON config file data.
 * 
 * @param[in|out]  _p_s_kernel_config Structure of stored data.
 * 
 * @return                    SAF_TRUE on Success.
 *                            SAF_FALSE otherwise.
 */
SAF_BOOL Free_Kernel_Config(s_Kernel_Config_t *_p_s_kernel_config);

/**
 * @brief  A function to translate from string hex representations to uint32_t values.
 * 
 * @param[in]   _p_c_hex          Input string hex.
 * @param[in]   num_bytes         Byte length in string.
 * @param[out]  _p_u32_dest_value Output number value.
 * 
 * @return                   SAF_TRUE on Success.
 *                           SAF_FALSE otherwise.
 */
SAF_BOOL Str_2_HEX(char *_p_c_hex, uint8_t num_bytes, uint32_t *_p_u32_dest_value);

/**
 * @briefs Reads the JSON config file and stores the content into a char stream.
 * 
 * @param[in]  _p_c_config_file_path Config file path.
 * @param[out]  _p_c_config_json      Output JSON stream
 * 
 * @return                       SAF_TRUE on Success.
 *                               SAF_FALSE otherwise.
 */
SAF_BOOL Json_To_Buffer(const char * _p_c_config_file_path, char ** _p_c_config_json);


/**
 * @brief Modify the size of memory segment for system page size alignment
 * 
 * @param[in]  size Intended allocation size
 * 
 * @return     Page aligned size
 */
unsigned long Align_For_Page_Size(unsigned long size);

#endif

/** @} (end addtogroup UTILS) *******************************************/
