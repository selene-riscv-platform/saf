#ifndef DATA_TYPES_H
#define DATA_TYPES_H

/* INCLUDES *******************************************************************/
#include <limits.h>
#include <stddef.h>
#include <stdint.h>

/**
 * @cond TEST_DOCS
 */

/***************************************************************************//**
 * @addtogroup SAF
 * @brief
 *   SAF data type definitions
 * @{
 ******************************************************************************/

/* DEFINES AND MACROS *********************************************************/

#ifndef __INTTYPES_H

/* SAF_UINT8 and SAF_INT8 */

#if (UCHAR_MAX >= 0x0ff)
  /** Entero SIN signo de 8 bits */
  typedef unsigned char                 SAF_UINT8;
  /** Entero CON signo de 8 bits */
  typedef signed char                   SAF_INT8;           
#elif (USHRT_MAX >= 0x0ff)
  typedef unsigned short                SAF_UINT8;        
  typedef signed short                  SAF_INT8;           
#elif (UINT_MAX >= 0x0ff)
  typedef unsigned int                  SAF_UINT8;        
  typedef signed int                    SAF_INT8;           
#elif (ULONG_MAX >= 0x0ff)
  typedef unsigned long                 SAF_UINT8;        
  typedef signed long                   SAF_INT8;           
#else
  #warning "Unsuported data type SAF_UINT8"
  #define SAF_UINT8           (unsupported data type)
  #warning "Unsuported data type SAF_INT8"
  #define SAF_INT8            (unsupported data type)
#endif


/* SAF_UINT16 and SAF_INT16 */

#if (UCHAR_MAX >= 0x0ffff)
  /** Entero SIN signo de 16 bits */
  typedef unsigned char                 SAF_UINT16;
  /** Entero CON signo de 16 bits */
  typedef signed char                   SAF_INT16;           
#elif (USHRT_MAX >= 0x0ffff)
  typedef unsigned short                SAF_UINT16;        
  typedef signed short                  SAF_INT16;           
#elif (UINT_MAX >= 0x0ffff)
  typedef unsigned int                  SAF_UINT16;        
  typedef signed int                    SAF_INT16;           
#elif (ULONG_MAX >= 0x0ffff)
  typedef unsigned long                 SAF_UINT16;        
  typedef signed long                   SAF_INT16;           
#else
  #warning "Unsuported data type SAF_UINT16"
  #define SAF_UINT16           (unsupported data type)
  #warning "Unsuported data type SAF_INT16"
  #define SAF_INT16            (unsupported data type)
#endif


/* SAF_UINT32 and SAF_INT32 */

#if (UCHAR_MAX >= 0x0ffffffffUL)
  /** Entero SIN signo de 32 bits */
  typedef unsigned char                 SAF_UINT32;
  /** Entero CON signo de 32 bits */
  typedef signed char                   SAF_INT32;           
#elif (USHRT_MAX >= 0x0ffffffffUL)
  typedef unsigned short                SAF_UINT32;        
  typedef signed short                  SAF_INT32;           
#elif (UINT_MAX >= 0x0ffffffffUL)
  typedef unsigned int                  SAF_UINT32;        
  typedef signed int                    SAF_INT32;           
#elif (ULONG_MAX >= 0x0ffffffffUL)
  typedef unsigned long                 SAF_UINT32;        
  typedef signed long                   SAF_INT32;           
#else
  #warning "Unsuported data type SAF_UINT32"
  #define SAF_UINT32           (unsupported data type)
  #warning "Unsuported data type SAF_INT32"
  #define SAF_INT32            (unsupported data type)
#endif

  /* TODO: ADD 64bit data types */

#endif /* __INTTYPES_H */

/* FLOATING POINT TYPES */
typedef float                           SAF_FLOAT;
typedef double                          SAF_DOUBLE;

/* SAF_UCHAR and SAF_CHAR */
typedef unsigned char                   SAF_UCHAR; /**< Unsigned char type */
typedef char                            SAF_CHAR; /**< Char type */


/* SAF_BOOL */
//typedef SAF_UINT8                         SAF_BOOL; /**< Boolean type */
typedef unsigned char								SAF_BOOL;

#define SAF_TRUE                          (0x01) /**< Boolean true */
#define SAF_FALSE                         (0x00) /**< Boolean false */


/* SAF_NULL */
#define SAF_NULL                          ((void *)0UL) /**< NULL type */


/* Unused Variables Macro to avoid gcc/g++ warnings */

#define SAF_UNUSED(x)                       ((void)(x))



/* PROPERTIES *****************************************************************/

/* Public property */
#ifndef __public__
  #define __public__         
#endif


/* Private property */
#ifndef __private__
  #define __private__ static     
#endif


/* Scope property */
#ifdef __cplusplus
  #define EXTERN extern "C" {
  EXTERN
#else
  #define EXTERN extern
#endif


#if defined ( __GNUC__ ) && !defined (__CC_ARM) /* GNU Compiler */
  #ifndef __weak
    /* __weak property is used to define overwrittable implementations */
    #define __weak   __attribute__((weak))
  #endif /* __weak */
  #ifndef __packed
    #define __packed __attribute__((__packed__))
  #endif /* __packed */
#endif /* __GNUC__ */



/* ALIASES ********************************************************************/

#define PRIVATE static
#define PUBLIC
#define CONST const

#ifndef  TRUE
  #define TRUE     (0x01)
#endif

#ifndef  FALSE
  #define FALSE    (0x00)
#endif

/* ENUMS **********************************************************************/

/** 
  * @brief  SAF_ Status structures definition  
  */  
typedef enum 
{
  E_SAF_OK            = 0x00U,
  E_SAF_TIMEOUT       = 0x03U,
  E_SAF_INVALID_ARG   = 0x04U,
  E_SAF_UNSUPPORTED   = 0x05U,
} e_SAF_Return_t;

/* STRUCTS ********************************************************************/

/**
 * @brief
 *   Generic callback definition
 * 
 * @param[in]  _e_status
 *   Callback's process execution status
 * 
 * @retval None
 */
typedef void (*SAF_Callback_t)(e_SAF_Return_t _e_status);

/* EXPORTED VARIABLES *********************************************************/

/** @} (end addtogroup SAF) ***************************************************/

/**
 * @endcond
 */

#ifdef __cplusplus
  };
#endif /* __cplusplus */

#endif /* SAF_TYPES_H */

/* EOF ************************************************************************/
