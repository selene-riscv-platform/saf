#ifndef SAF_H
#define SAF_H

#include "data_types.h"

//#define SAF_DEBUG 1
//#define COMMIT

#ifdef SAF_DEBUG
  #if (SAF_DEBUG > 0) && (SAF_DEBUG < 4) /* Debug level 1 */
    #pragma message "Enabled Debug Level 1"
    #define SAF_DEBUG_LEVEL_1
  #endif

  #if (SAF_DEBUG > 1) && (SAF_DEBUG < 4) /* Debug level 2 */
    #pragma message "Enabled Debug Level 2"
    #define SAF_DEBUG_LEVEL_2
  #endif
  
  #if (SAF_DEBUG > 2) && (SAF_DEBUG < 4) /* Debug level 3 */
    #pragma message "Enabled Debug Level 3"
    #define SAF_DEBUG_LEVEL_3
  #endif
#endif

#ifdef SAF_DEBUG_LEVEL_1
  #define DEBUG_LOG_1(fmt, args...) fprintf( stderr,"SAF_DEBUG: %s:%d:%s(): " fmt, __FILE__, __LINE__, __func__, ##args )
#else
  #define DEBUG_LOG_1(fmt, args...)
#endif

#ifdef SAF_DEBUG_LEVEL_2
  #define DEBUG_LOG_2(fmt, args...) fprintf( stderr,"SAF_DEBUG: %s:%d:%s(): " fmt, __FILE__, __LINE__, __func__, ##args )
#else
  #define DEBUG_LOG_2(fmt, args...)
#endif

#ifdef SAF_DEBUG_LEVEL_3
  #define DEBUG_LOG_3(fmt, args...) fprintf( stderr,"SAF_DEBUG: %s:%d:%s(): " fmt, __FILE__, __LINE__, __func__, ##args )
#else
  #define DEBUG_LOG_3(fmt, args...)
#endif

#define DEBUG_WARN(fmt, args...) fprintf( stderr,"SAR Warning: %s:%d:%s(): " fmt, __FILE__, __LINE__, __func__, ##args )
#define DEBUG_ERR(fmt, args...) fprintf( stderr,"SAR Error: %s:%d:%s(): " fmt, __FILE__, __LINE__, __func__, ##args )


/* Scope property */
#ifdef __cplusplus
  #define EXTERN extern "C" {
  EXTERN
#else
  #define EXTERN extern
#endif


/***************************************************************************//**
 * @addtogroup SAF
 * @brief
 *   Selene Accelerator Framework
 * @{
 ******************************************************************************/

/**
 * @brief SAF Kernel Handle Opaque Pointer
 */
typedef struct SAF_Kernel_Handle_Typedef *SAF_Kernel_Handle_T;

/**
 * @brief SAF Buffer Handle Opaque Pointer
 */
typedef struct SAF_Buffer_Typedef *SAF_Buffer_T;

/**
 * @brief SAF Kernel status enum
 */
typedef enum {
  E_SAF_UNCONFIGURED,
  E_SAF_READY,
  E_SAF_IDLE,
  E_SAF_DONE,
  E_SAF_ERROR,
  E_SAF_BUSY
} SAF_Kernel_Status_Typedef;


/*Function declaration*/

/**
 * @brief Gets SAF Config file.
 * 
 * @return  SAF_TRUE on success.
 *          SAF_FALSE otherwise.
 */
SAF_BOOL SAF_init(void);


/**
 * @brief Allocates buffer of size _s_bytes of physical memory
 * 
 * @param[in]  _s_bytes Min size of memery to allocate
 * 
 * @return          SAF_Buffer_T handle on Success.
 *                  NULL pointer otherwise.
 */
SAF_Buffer_T SAF_bufferAlloc(size_t _s_bytes);

/**
 * @brief Adds value to buffer in defined memory index
 * 
 * @param[in]  _s_buff    Buffer handle
 * @param[in]  _u64_index Index to store value
 * @param[in]  _u32_value  32bit value to be stored
 * 
 * @return  SAF_TRUE on success.
 *          SAF_FALSE otherwise.
 */
SAF_BOOL SAF_bufferAddItem(SAF_Buffer_T _s_buff, uint64_t _u64_index, uint32_t u32_value);


/**
 * @brief Gets item on define memory index from buffer
 * 
 * @param[in]   _s_buff       Buffer handle
 * @param[in]   _u64_index    Index of item
 * @param[out]  _p_u32_value  32bit pointer value to be read to
 * 
 * @return  SAF_TRUE on success.
 *          SAF_FALSE otherwise.
 */
SAF_BOOL SAF_bufferGetItem(SAF_Buffer_T _s_buff, uint64_t _u64_index, uint32_t * _p_u32_value);


/**
 * @brief      Reads _u32_size bytes from _u32_offset of _p_data pointer
 *
 * @param[in]  _s_buff      Buffer handle
 * @param[in]  _u32_offset  Offset of bytes from buffer start addres
 * @param[in]  _u32_size    Amount of bytes to read from buffer
 * @param      _p_data      Pointer of memory to store readed data
 *
 * @return  SAF_TRUE on success.
 *          SAF_FALSE otherwise.
 */
SAF_BOOL SAF_bufferRead(SAF_Buffer_T _s_buff, uint32_t _u32_offset, uint32_t _u32_size, void* _p_data);

/**
 * @brief      Writes _u32_size bytes from _p_data pointer to _u32_offset bytes of Buffer start address
 *
 * @param[in]  _s_buff      Buffer handle
 * @param[in]  _u32_offset  Offset of bytes from buffer start addres
 * @param[in]  _u32_size    Amount of bytes to write to buffer
 * @param      _p_data      Pointer of memory to read data from
 *
 * @return  SAF_TRUE on success.
 *          SAF_FALSE otherwise.
 */
SAF_BOOL SAF_bufferWrite(SAF_Buffer_T _s_buff, uint32_t _u32_offset, uint32_t _u32_size, void* _p_data);

/**
 * @brief      { function_description }
 *
 * @param[in]  _s_buff     The s buffer
 * @param[in]  _u64_index  The u 64 index
 *
 * @return     { description_of_the_return_value }
 */
uint64_t SAF_bufferGetPhyAddr(SAF_Buffer_T _s_buff, uint64_t _u64_index);

/**
 * @brief      { function_description }
 *
 * @param[in]  _s_buff     The s buffer
 * @param[in]  _u64_index  The u 64 index
 *
 * @return     { description_of_the_return_value }
 */
uint64_t SAF_bufferGetVirtAddr(SAF_Buffer_T _s_buff, uint64_t _u64_index);

/**
 * @brief      { function_description }
 *
 * @param[in]  _s_buff  The s buffer
 *
 * @return     { description_of_the_return_value }
 */
uint32_t SAF_bufferGetSize(SAF_Buffer_T _s_buff);


/**
 * @brief Frees physical memory of SAF_Buffer_T
 * 
 * @param[in] _s_buffer Buffer handle
 */
void SAF_bufferFree(SAF_Buffer_T _s_buffer);

/**
 * [SAF_requestIRQ description]
 * @param  callback_func [description]
 * @return               [description]
 */
uint32_t SAF_requestIRQ(void (*f)(uint32_t));


/**
 * @brief Search for kernel config metadata and allocates control
 *        structure.
 *  
 * @param[in]  _pc_kernel_id Kernel ID string.
 *
 * @return        SAF_Kernel_Handle on success.
 *                NULL pointer otherwise.
 */
SAF_Kernel_Handle_T SAF_getKernel(const char *_p_c_kernel_id);


void SAF_free_Kernel(SAF_Kernel_Handle_T _s_handle);


/**
 * @brief Configures arguments on kernel
 *
 * @param[in]  _s_handle    Kernel handler structure pointer.
 * @param[in]  _pc_arg_name Name of the argument to set.
 * @param[in]  _u32_value   Value of the argument to set.
 * 
 * @return                  SAF_TRUEon Success.
 *                          SAF_FALSE otherwise.
 */
SAF_BOOL SAF_setArg(SAF_Kernel_Handle_T _s_handle, char *_pc_arg_name, void * void_ptr);

/**
 * @brief Configures arguments on kernel
 *
 * @param[in]  _s_handle    Kernel handler structure pointer.
 * @param[in]  _u32_index   Index of the argument to set.
 * @param[in]  _u32_value   Value of the argument to set.
 * 
 * @return                  SAF_TRUEon Success.
 *                          SAF_FALSE otherwise.
 */
SAF_BOOL SAF_setArgByIndex(SAF_Kernel_Handle_T _s_handle, uint32_t _u32_index, void * void_ptr);


/**
 * @brief Dumps argument data to kernel registers and sets the required flags to fire the kernel.
 * @param[in]  _s_handle       Kernel handler structure pointer.
 * @param[in]  _u32_timeout_us Timeout of kernel run status polling.
 * 
 * @return                 SAF_TRUEon Success.
 *                         SAF_FALSE otherwise.
 */
SAF_BOOL SAF_runKernel(SAF_Kernel_Handle_T _s_handle, SAF_UINT32 _u32_timeout_us);


/**
 * @brief Polls kernel status info from control register.
 * 
 * @param[in]  _s_handle Kernel handler structure.
 * 
 * @return           SAF_Kernel_Status_Typedef status.
 */
SAF_Kernel_Status_Typedef SAF_getKernelSatus(SAF_Kernel_Handle_T _s_handle);


/**
 * @brief      Returns signal name by its index  
 *
 * @param[in]  _s_handle     Kernel handler structure pointer.
 * @param[in]  _u8_arg_idx   Index of the signal.
 * @param[out] _pc_arg_name  Char pointer to store signal name. 
 *
 * @return                  SAF_TRUE on Success.
 *                          SAF_FALSE otherwise.
 */
SAF_BOOL SAF_getArgNameByIndex(SAF_Kernel_Handle_T _s_handle, uint8_t _u8_arg_idx, char *_pc_arg_name);


/**
 * @cond TEST_DOCS
 */


/**
 * @brief Function to test address manipulation and shifting operations.
 * 
 * @param  _s_handle Kernel handler structure.
 * 
 * @return           TRUE if successful.
 *                   FALSE otherwise.
 */
SAF_BOOL SAF_Debug_Offsets_Shifts(SAF_Kernel_Handle_T _s_handle);

/**
 * @brief DEBUG: Performs physical memory reading of SAF_Buffer index
 * 
 * @param[in]  _s_buff    Buffer handle
 * @param[in]  _u64_index Memory index to read
 * 
 * @return           TRUE if successful.
 *                   FALSE otherwise.
 */
SAF_BOOL SAF_Debug_Buffer_Physical_Mem(SAF_Buffer_T _s_buff, uint64_t _u64_index);

/**
 * @brief DEBUG: Print the controls registers of a kernel
 * 
 * @param[in]  _s_handle Kernel handler structure.
 * 
 * @return           TRUE if successful.
 *                   FALSE otherwise.
 */
SAF_BOOL SAF_Debug_Kernel(SAF_Kernel_Handle_T _s_handle);

/**
 * [SAF_getKernelFakeTest description]
 * @param  _p_c_kernel_id [description]
 * @return                [description]
 */
// SAF_Kernel_Handle_T SAF_getKernelFakeTest(const char *_p_c_kernel_id);


/**
 * [SAF_dump_to_u32_ptr  description]
 * @param  _value [description]
 * @return        [description]
 */
uint32_t SAF_dump_to_u32_ptr (float _f_value);

/**
 * [SAF_get_from_u32_ptr  description]
 * @param  _u32_value [description]
 * @return            [description]
 */
float SAF_get_from_u32_ptr (uint32_t _u32_value);

/**
 * @brief Print the library version
 * 
 * @param[in] 
 * 
 * @return
 * 
 */
void SAF_print_ver(void);

/**
 * @endcond
 */

#ifdef __cplusplus
  };
#endif /* __cplusplus */

#endif

/** @} (end addtogroup SAF) *******************************************/

