#ifndef SAF_OCL_H
#define SAF_OCL_H

//#include <CL/opencl.h>
#include "saf.h"
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <vector>
#include <cstdlib>
#include <cstring>

/* Definitions */
#define VECTOR_CLASS std::vector

#define OCL_CHECK(error,call)                                       \
    call;                                                           \
    if (error != CL_SUCCESS) {                                      \
      printf("%s:%d Error calling " #call ", error code is: %d\n",  \
              __FILE__,__LINE__, error);                            \
      exit(EXIT_FAILURE);                                           \
    }   
/* End Definitions */



/* Typedefinitions */

/* scalar types  */
typedef int8_t          cl_char;
typedef uint8_t         cl_uchar;
typedef int16_t         cl_short    __attribute__((aligned(2)));
typedef uint16_t        cl_ushort   __attribute__((aligned(2)));
typedef int32_t         cl_int      __attribute__((aligned(4)));
typedef uint32_t        cl_uint     __attribute__((aligned(4)));
typedef int64_t         cl_long     __attribute__((aligned(8)));
typedef uint64_t        cl_ulong    __attribute__((aligned(8)));

typedef uint16_t        cl_half     __attribute__((aligned(2)));
typedef float           cl_float    __attribute__((aligned(4)));
typedef double          cl_double   __attribute__((aligned(8)));
typedef cl_uint         cl_bool; /* WARNING!  Unlike cl_ types in cl_platform.h, cl_bool is not guaranteed to be the same size as the bool in kernels. */ 

/* End Typedefinitions */


/* Enumerations */
enum {
   CL_SUCCESS = 0,
   CL_ERROR = -1 
};

enum {
   CL_TRUE = 1,
   CL_FALSE = 0 
};
/* End Enumerations */




namespace cl {


class Event;
class Buffer;
class Context;
class CommandQueue;
class Program;
class Kernel;
class Device;
class Platform;

class Event {
public:
    Event(){}
    ~Event(){}
};


/**
 * @brief      Wrapper for Context class interface
 */
class Context {

public:
    Context(){}

    /**
     * @brief      Constructs a new instance.
     *
     * @param[in]  devices     (Unused) The devices
     * @param[in]  properties  (Unused) The properties
     * @param[in]  cb          (Unused) { parameter_description }
     * @param[in]  data        (Unused) The data
     * @param[out] err         Error status pointer.
     */
    Context(void *devices,
            void *properties = NULL,
            void *cb = NULL,
            void *data = NULL,
            cl_int *err = NULL) {

        /* Initialize SAF */
        if (SAF_init() != SAF_TRUE)
        {
            *err = CL_ERROR;
        }

        *err = CL_SUCCESS;
    }

    /**
     * @brief      Destroys the object.
     */
    ~Context(){}
};


/**
 * @brief      Wrapper for Program class interface
 */
class Program {
public:
    Program(){}
    /**
     * @brief      Constructs a new instance.
     *
     * @param[in]  context       The context
     * @param[in]  devices       (Unused)The devices
     * @param[in]  binaries      (Unused)The binaries
     * @param[in]  binaryStatus  (Unused)The binary status
     * @param[out] err           The error
     */
    Program(const Context& context,
        void *devices = NULL,
        void *binaries = NULL,
        void *binaryStatus = NULL,
        cl_int *err = NULL) {}

    /**
     * @brief      Destroys the object.
     */
    ~Program() {}
};



/**
 * @brief      This class describes a buffer object.
 */
class Buffer
{
private:

    SAF_Buffer_T s_handle;

    /**
     * @brief      Internal constructor of SAF Buffer
     *
     * @param[in]  size  Size in bytes of Buffer memory.
     * @param[out] err   Error status pointer.
     *
     * @return     Reference of self object.
     */
    Buffer& Buffer_Constructor(::size_t size, cl_int *err = NULL) {

        this->s_handle = SAF_bufferAlloc(size);

        *err = (this->s_handle == NULL) ? CL_ERROR : CL_SUCCESS;

        return *this;
    }

public:

    Buffer(){}

    /**
     * @brief      Constructs a new instance of Buffer.
     *
     * @param[in]  context   A valid context object.
     * @param[in]  flags     (Unused).
     * @param[in]  size      Size in bytes of Buffer memory.
     * @param[in]  host_ptr  (Unused).
     * @param[out] err       Error status pointer.
     */
    Buffer(
        const Context& context,
        cl_uint flags,
        ::size_t size,
        void* host_ptr = NULL,
        cl_int* err = NULL){

        this->s_handle = NULL;
        Buffer_Constructor(size, err);

    }

    /**
     * @brief      Destroys the object.
     */
    ~Buffer(){}


    /**
     * @brief      Adds an item to Buffer.
     *
     * @param[in]  index  The index of the item
     * @param      value  Item value to store
     *
     * @return     returns CL_SUCCESS if the function is executed successfully. Otherwise, it returns CL_ERROR.
     */
    cl_int addItem(cl_uint index, uint32_t value) {
        return ((SAF_TRUE == SAF_bufferAddItem(this->s_handle, index, value)) ? CL_SUCCESS : CL_ERROR);
    }

    /**
     * @brief      Gets an item from Buffer.
     *
     * @param[in]  index  The index of the item
     * @param[out] value  Pointer to store the item
     *
     * @return     returns CL_SUCCESS if the function is executed successfully. Otherwise, it returns CL_ERROR.
     */
    cl_int getItem(cl_uint index, uint32_t *value) {
        return ((SAF_TRUE == SAF_bufferGetItem(this->s_handle, index, value)) ? CL_SUCCESS : CL_ERROR);
    }

    /**
     * @brief      Reads bytes from buffer memory
     *
     * @param[in]  offset    Memory offset from buffer start
     * @param[in]  bytes     Bytes to read
     * @param[out] data_ptr  Pointer to store readed data
     *
     * @return     returns CL_SUCCESS if the function is executed successfully. Otherwise, it returns CL_ERROR.
     */
    cl_int read(cl_uint offset, cl_uint bytes, void *data_ptr) {
        return ((SAF_TRUE == SAF_bufferRead(this->s_handle, offset, bytes, data_ptr)) ? CL_SUCCESS : CL_ERROR);
    }

    /**
     * @brief      Writes bytes to buffer memory
     *
     * @param[in]  offset    Memory offset from buffer start
     * @param[in]  bytes     Bytes to write
     * @param[out] data_ptr  Pointer to read data from
     *
     * @return     returns CL_SUCCESS if the function is executed successfully. Otherwise, it returns CL_ERROR.
     */
    cl_int write(cl_uint offset, cl_uint bytes, void *data_ptr) {
        return ((SAF_TRUE == SAF_bufferWrite(this->s_handle, offset, bytes, data_ptr)) ? CL_SUCCESS : CL_ERROR);
    }

    /**
     * @brief      Gets the buffer handle.
     *
     * @return     The buffer handle.
     */
    SAF_Buffer_T getBufferHandle() {

        return this->s_handle;
    }

    /**
     * @brief      Gets the phy address.
     *
     * @param[in]  index  The index
     *
     * @return     The phy address.
     */
    uint64_t getPhyAddr(cl_uint index) {

        if (this->s_handle == NULL) {
            return 0;
        }

        return SAF_bufferGetPhyAddr(this->s_handle, index);
    }

    /**
     * @brief      Gets the virt address.
     *
     * @param[in]  index  The index
     *
     * @return     The virt address.
     */
    uint64_t getVirtAddr(cl_uint index) {

        if (this->s_handle == NULL) {
            return 0;
        }

        return SAF_bufferGetVirtAddr(this->s_handle, index);
    }

    uint32_t getSize(void) {
        return SAF_bufferGetSize(this->s_handle);
    }

    /**
     * @brief      Free allocated buffer memory
     */
    void free(void) {

        if (this->s_handle == NULL) {
            return;
        }

        SAF_bufferFree(this->s_handle);
    }
};



/**
 * @brief      Wrapper for Kernel Class interface for cl_kernel.
 */
class Kernel
{

private:

    SAF_Kernel_Handle_T s_handle;
    const char* name;
    SAF_BOOL b_saf_init;


    /**
     * @brief      Kernel's SAF constructor
     *
     * @param[in]  name  ID of kernel description.
     * @param[out] err   Error status pointer.
     *
     * @return     { description_of_the_return_value }
     */
    Kernel& Kernel_Constructor(const char* name, cl_int* err) {

        if (name == nullptr)
        {
            *err = CL_ERROR;
            return *this;
        }

        this->s_handle = SAF_getKernel(name);

        if (this->s_handle == NULL)
        {
            *err = CL_ERROR;
            return *this;
        }

        this->name = name;
        this->b_saf_init = SAF_TRUE;
        *err = CL_SUCCESS;

        return *this;
    }

public:

    Kernel(){}

    /**
     * @brief      Constructs a new instance of Kernel object.
     *
     * @param[in]  program  (Unused).
     * @param[in]  name     ID of kernel description.
     * @param[out] err      Error status pointer.
     */
    Kernel(const Program& program, const char* name, cl_int* err = NULL) {
        this->b_saf_init = SAF_FALSE;
        Kernel_Constructor(name, err);
    }

    /**
     * @brief      Destroys the object.
     */
    ~Kernel(){}

    /**
     * @brief      Sets argument on kernel's registers
     *
     * @param[in]  index  Index of signal in kernels register.
     * @param[in]  value  Value to set on signal.
     *
     * @return     returns CL_SUCCESS if the function is executed successfully. Otherwise, it returns CL_ERROR.
     */
    cl_int setArg(cl_uint index, void *value)
    {
        /* Eval initialization */
        if (!this->b_saf_init)
        { return CL_ERROR; }

        if (SAF_TRUE == SAF_setArgByIndex(this->s_handle, index, value))
            { return CL_SUCCESS; }
        else
            { return CL_ERROR; }
    }

    /**
     * @brief      Sets argument on kernel's registers
     *
     * @param[in]  index  Index of signal in kernels register.
     * @param[in]  value  Value to set on signal.
     *
     * @return     returns CL_SUCCESS if the function is executed successfully. Otherwise, it returns CL_ERROR.
     */
    cl_int setArg(cl_uint index, float value)
    {
        float tmp_value;

        /* Eval initialization */
        if (!this->b_saf_init)
        { return CL_ERROR; }

        tmp_value = value;

        if (SAF_TRUE == SAF_setArgByIndex(this->s_handle, index, &tmp_value))
            { return CL_SUCCESS; }
        else
            { return CL_ERROR; }
    }

    /**
     * @brief      Sets argument on kernel's registers
     *
     * @param[in]  index  Index of signal in kernels register.
     * @param[in]  value  Value to set on signal.
     *
     * @return     returns CL_SUCCESS if the function is executed successfully. Otherwise, it returns CL_ERROR.
     */
    cl_int setArg(cl_uint index, cl_int value)
    {
        cl_int tmp_value;

        /* Eval initialization */
        if (!this->b_saf_init)
        { return CL_ERROR; }

        tmp_value = value;

        if (SAF_TRUE == SAF_setArgByIndex(this->s_handle, index, &tmp_value))
            { return CL_SUCCESS; }
        else
            { return CL_ERROR; }
    }

    /**
     * @brief      Sets argument on kernel's registers
     *
     * @param[in]  index  Index of signal in kernels register.
     * @param[in]  value  Value to set on signal.
     *
     * @return     returns CL_SUCCESS if the function is executed successfully. Otherwise, it returns CL_ERROR.
     */
    cl_int setArg(cl_uint index, cl_uint value)
    {
        cl_uint tmp_value;

        /* Eval initialization */
        if (!this->b_saf_init)
        { return CL_ERROR; }

        tmp_value = value;

        if (SAF_TRUE == SAF_setArgByIndex(this->s_handle, index, &tmp_value))
            { return CL_SUCCESS; }
        else
            { return CL_ERROR; }
    }


    /**
     * @brief      Sets argument on kernel's registers
     *
     * @param[in]  index   Index of signal in kernels register.
     * @param[in]  buffer  Buffer object to set on signal.
     *
     * @return     returns CL_SUCCESS if the function is executed successfully. Otherwise, it returns CL_ERROR.
     */
    cl_int setArg(cl_uint index, Buffer& buffer)
    {
        SAF_Buffer_T value;
        cl_int err;

        /* Eval initialization */
        if (!this->b_saf_init)
        { return CL_ERROR; }

        value = buffer.getBufferHandle();

        if (value == NULL)
            { return CL_ERROR; }


        if (SAF_TRUE == SAF_setArgByIndex(this->s_handle, index, value))
            { return CL_SUCCESS; }
        else
            { return CL_ERROR; }

    }

    /**
     * @brief      Executes a kernel.
     *
     * @param[in]  timeout_us  Timeout in microseconds. If 0 will wait forever.
     *
     * @return     returns CL_SUCCESS if the function is executed successfully. Otherwise, it returns CL_ERROR.
     */
    cl_int run(cl_uint timeout_us = 0) {

        /* Eval Kernel object initialization */
        if (this->b_saf_init == SAF_FALSE)
        {
            return CL_ERROR;
        }

        /* Run Kernel */
        if (SAF_TRUE != SAF_runKernel(this->s_handle, timeout_us))
        {
            return CL_ERROR;
        }

        return CL_SUCCESS;
    }


    void free(void) {
        /* Eval Kernel object initialization */
        if (this->b_saf_init == SAF_FALSE)
        {
            return;
        }

        SAF_free_Kernel(this->s_handle);

        this->b_saf_init = SAF_FALSE;
    }
};



/**
 * @brief      This class describes a queue of commands.
 */
class CommandQueue
{
private:

    Kernel kernel;
    cl_bool b_kernel_set;


public:

    /**
     * @brief      Default constructor.
     */
    CommandQueue(){
        /* Clears the kernel set for run at object creation */
        this->b_kernel_set = CL_FALSE;
    }

    /**
     * @brief      Constructs a new instance of CommandQueue.
     *
     * @param[in]  context     A valid context object.
     * @param      device      (Unused).
     * @param      properties  (Unused).
     * @param[out] err         Error status pointer.
     */
    CommandQueue(
        const Context& context,
        void *device = NULL,
        void *properties = NULL,
        cl_int *err = NULL) {

        this->b_kernel_set = CL_FALSE;
        *err = CL_SUCCESS;
    }

    /**
     * @brief      Destroys the object.
     */
    ~CommandQueue(){}

    /**
     * @brief      Enqueues a command to execute a kernel on a device.
     *
     * @param[in]  kernel  A valid kernel object.
     * @param      events  (Unused).
     * @param      event   (Unused).
     *
     * @return     returns CL_SUCCESS if the function is executed successfully. Otherwise, it returns CL_ERROR.
     */
    cl_int enqueueTask(
        Kernel& kernel,
        void* events = NULL,
        void* event = NULL) {

        // Kernel run
        this->kernel = kernel;
        this->b_kernel_set = CL_TRUE;

        return CL_SUCCESS;
    }

    /**
     * @brief       Enqueue commands to write to a buffer object from host memory.
     *
     * @param[out]  buffer    Refers to a valid buffer object.
     * @param[in]   blocking  Indicates if the write operations are blocking (CL_TRUE) or nonblocking (CL_FALSE).
     *                        (currently only blocking allowed).
     * @param[in]   offset    The offset in Bytes of buffer object to write to.
     * @param[in]   size      The size in bytes of data being written.
     * @param[in]   ptr       The pointer to buffer in host memory where data is to be written from.
     * @param       events    (Unused).
     * @param       event     (Unused).
     *
     * @return      returns CL_SUCCESS if the function is executed successfully. Otherwise, it returns CL_ERROR.
     */
    cl_int enqueueWriteBuffer(
        Buffer& buffer,
        cl_bool blocking,
        cl_int offset,
        cl_int size,
        void *ptr,
        void *events = NULL,
        void *event = NULL) {

        // cl_int retval = CL_SUCCESS;
        // uint32_t *u32_ptr = (uint32_t *)ptr;

        /* Eval params */
        //printf("enqueueWriteBuffer\n");
        //printf("offset: 0x%X \n", offset);
        //printf("size: 0x%X \n", size);
        //printf("ptr: 0x%lX \n", ptr);

        if (
            offset < 0      ||
            size <= 0       ||
            ptr == nullptr
            ) 
        {
            return CL_ERROR;
        }

        /* Add individual size elements to buffer object */
        // for (cl_int i = 0; i < size; i++)
        // {

        //     // printf("buffer.addItem(%d, 0x%X - %d)\n", offset+i, (u32_ptr + i), *(u32_ptr + i));

        //      /* Offset param is translated to index of data size object NOT BYTES */
        //     if (CL_ERROR == buffer.addItem(offset+i, *(u32_ptr + i)))
        //     {
        //         retval = CL_ERROR;
        //         break;
        //     }
        // }

        return buffer.write(offset, size, ptr);
    }

    /**
     * @brief       Enqueue commands to read from a buffer object to host memory.
     *
     * @param[in]   buffer    Refers to a valid buffer object.
     * @param[in]   blocking  Indicates if the write operations are blocking (CL_TRUE) or nonblocking (CL_FALSE).
     *                        (currently only blocking allowed).
     * @param[in]   offset    The offset in Bytes of buffer object to read from.
     * @param[in]   size      The size in bytes of data being written.
     * @param[out]  ptr       The pointer to buffer in host memory where data is to be read into.
     * @param       events    (Unused).
     * @param       event     (Unused).
     *
     * @return      returns CL_SUCCESS if the function is executed successfully. Otherwise, it returns CL_ERROR.
     */
    cl_int enqueueReadBuffer(
        Buffer& buffer,
        cl_bool blocking,
        cl_int offset,
        cl_int size,
        void *ptr,
        void *events = NULL,
        void *event = NULL) {

        //cl_int retval = CL_SUCCESS;
        //uint32_t *u32_ptr = (uint32_t *)ptr;

        /* Eval params */
        //printf("enqueueReadBuffer\n");
        //printf("offset: 0x%X \n", offset);
        //printf("size: 0x%X \n", size);
        //printf("ptr: 0x%lX \n", ptr);
        if (
            offset < 0      ||
            size <= 0       ||
            ptr == nullptr
            ) 
        {
            return CL_ERROR;
        }

        /* Get individual size elements to buffer object */
        // for (cl_int i = 0; i < size; ++i)
        // {
        //     /* Offset param is translated to index of data size object NOT BYTES */
        //     if (CL_ERROR == buffer.getItem(offset+i, (u32_ptr + i)))
        //     {
        //         retval = CL_ERROR;
        //         break;
        //     }
        // }

        return buffer.read(offset, size, ptr);
    }

    /**
     * @brief       Enqueues a command to copy a buffer object to another buffer object.
     *
     * @param[in]   src         Refers to a valid buffer object.
     * @param[out]  dst         Refers to a valid buffer object.
     * @param[in]   src_offset  The offset where to begin copying data from src buffer.
     * @param[in]   dst_offset  The offset where to begin copying data into dst buffer.
     * @param[in]   size        Refers to the size in bytes to copy.
     * @param       events      (Unused).
     * @param       event       (Unused).
     *
     * @return      returns CL_SUCCESS if the function is executed successfully. Otherwise, it returns CL_ERROR.
     */
    cl_int enqueueCopyBuffer(
        Buffer& src,
        Buffer& dst,
        ::size_t src_offset,
        ::size_t dst_offset,
        ::size_t size,
        void *events = NULL,
        void *event = NULL) {

        cl_int retval = CL_SUCCESS;
        uint32_t *u32_ptr;

        /* Eval params */
        if (
            src_offset < 0  ||
            dst_offset < 0  ||
            size <= 0
            ) 
        {
            return CL_ERROR;
        }

        /* Get individual size elements to buffer object */
        for (cl_int i = 0; i < size; ++i)
        {
            /* Offset params are translated to index of data size object NOT BYTES */
            /* Deta data from source */
            if (CL_ERROR == src.getItem(src_offset+i, u32_ptr))
            {
                retval = CL_ERROR;
                break;
            }

            if (CL_ERROR == dst.addItem(dst_offset+i, *u32_ptr))
            {
                retval = CL_ERROR;
                break;
            }
        }

        return retval;

    }

    /**
     * @brief      Blocks until all previously queued OpenCL commands in a command-queue are issued to the associated device and have completed.
     * 
     * @details    Currently is used in combination with CommandQueue::enqueueTask to execute a Kernel object.
     *
     * @return     returns CL_SUCCESS if the function is executed successfully. Otherwise, it returns CL_ERROR.
     */
    cl_int finish(cl_uint timeout_us = 1000) {

        cl_int retval;

        /* Eval if a kernel task has been enqueued */
        if (this->b_kernel_set != CL_TRUE)
        {
            printf("this->b_kernel_set != CL_TRUE\n");
            return CL_ERROR;
        }

        /* Exec kernel */
        retval = this->kernel.run(timeout_us);

        /* Remove kernel */
        this->kernel.~Kernel(); /* TODO check */
        this->b_kernel_set = CL_FALSE;
        return retval;
    }
    
};

} /* End of namespace cl */

#endif