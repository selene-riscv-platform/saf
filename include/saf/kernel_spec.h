/*
 * accel_spec.h
 *
 *  Created on: 4 jun. 2021
 *      Author: mjonian
 */

#include <stdint.h>

/***************************************************************************//**
 * @addtogroup SAF
 * @{
 ******************************************************************************/

#ifndef KERNEL_SPEC_H_
#define KERNEL_SPEC_H_

/* NOTE: Pass as a Makefile Flag(?) */
#define KERNEL_CONFIG_FILE ("/etc/saf.d/config.json")
//#define KERNEL_CONFIG_FILE ("./config.json")

#define GLOBAL_IRQ_ENABLE_OFFSET 0x04
#define IP_IRQ_ENABLE_OFFSET 0x08
#define IP_IRQ_STATUS_OFFSET 0x0c


/**
 * @brief Signal type enumeration
 */
typedef enum {
	E_KERNEL_SIGNAL_BUFFER,
	E_KERNEL_SIGNAL_VALUE
} Kernel_Signal_Type_Typedef;

typedef enum {
  E_KERNEL_SIGNAL_DATA_TYPE_UINT32,
  E_KERNEL_SIGNAL_DATA_TYPE_UINT64,
  E_KERNEL_SIGNAL_DATA_TYPE_FLOAT
} Kernel_Signal_Data_Type_Typedef;


/**
 * @brief Kernel config register structure abstraction 
 */
typedef struct Kernel_Config_Ctrl_Reg_Typedef {
  uint8_t offset;
  struct {
    uint8_t ap_start;
    uint8_t ap_done;
    uint8_t ap_idle;
    uint8_t ap_ready;
    uint8_t ap_restart;
    uint8_t ap_continue;
  } bit_pos;
} s_Kernel_Config_Ctrl_Reg_t;


/**
 * @brief Kernel signal structure abstraction
 */
typedef struct Kernel_Config_Signal_Typedef {
  char *name;
  uint16_t offset;
  uint8_t size_bits;
  uint8_t pos;
  Kernel_Signal_Type_Typedef type;
  Kernel_Signal_Data_Type_Typedef data_type;
} s_Kernel_Config_Signal_t;


/**
 * @brief Kernel dynamic internal structure abstraction
 */
typedef struct Kernel_Config_Typedef {
  char *name;
  uint32_t base_addr;
  uint32_t offset;
  uint8_t signals_size;
  s_Kernel_Config_Ctrl_Reg_t control_reg;
  s_Kernel_Config_Signal_t * signals; /* runtime malloc(n) signal ports */
} s_Kernel_Config_t;

/** @} (end addtogroup SAF) ***************************************************/


#endif /* KERNEL_SPEC_H_ */
