#include <sys/mman.h>
#include <sys/wait.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <semaphore.h>
#include <pthread.h>

#include <saf/saf.h>
#include <saf/data_types.h>
#include <saf/utils.h>
#include <saf/kernel_spec.h>
#include "mad_ioctl.h"
#include "ird_ioctl.h"

#include "errno.h"
#include <assert.h>
extern int errno;

/* Check if the bit n is set or reset in x */
#define CHECK_BIT(x, n) ( ((x)>>(n)) & 1 )
/* Change the bit n to b in x*/
#define CHANGE_BIT(x, n, b) ( ( x & ~(1UL << n) ) | (b << n) )


/***************************************************************************//**
 * @addtogroup SAF
 * @{
 ******************************************************************************/

#define BASE_ADDR_32BIT_OFFSET(offset) ((offset)/4)

/**
 * @brief SAF Buffer handle internal structure
 */
struct SAF_Buffer_Typedef {
  struct mad_mo mo;
  void *user_virtaddr;
  SAF_Buffer_T next;
};


/**
 * @brief Kernel signal's value internal link structure
 */
typedef struct SAF_Kernel_Signals_Typedef {
  uint8_t signal_index;
  uint32_t signal_value;
} SAF_Kernel_Signals_T;


/**
 * @brief Structure for handling acceleration kernel registers and data
 */
struct SAF_Kernel_Handle_Typedef {
  SAF_BOOL b_init;
  SAF_Kernel_Status_Typedef e_status;
  s_Kernel_Config_t s_kernel_config;

  void *p_mapped_addr;
  
  uint8_t signals_size;
  SAF_Kernel_Signals_T *as_kernel_signals;

  SAF_Kernel_Handle_T next;

};

/**
 * @brief Structure for handling interruption occurrence
 */
struct ird_io io;


typedef struct ird_context {
  SAF_Kernel_Handle_T s_handle;
  int32_t ird_fd;
} ird_context_t;

sem_t queue;

pthread_t interruption_thread;

SAF_BOOL SAF_Debug_Physical_Mem(void * _p_baseAddr, uint16_t _offset);

// uint32_t ird_fd = 0;
ird_context_t ird_ctx;

SAF_BOOL beExit;

/**
 * Kernel handlers linked list head
 */
static SAF_Kernel_Handle_T vg_kernel_handle_instance = NULL;

/**
 * Buffer handlers linked list head
 */
static SAF_Buffer_T vg_buffer_handle_instance = NULL;

/**
 * Kernel handlers allocated instances counter
 */
static uint32_t vg_kernel_handle_instances = 0;

/**
 * JSON config file content buffer pointer
 */
const char *vg_p_c_config_json = NULL;

/* Local functions */

/**
 * @brief Calls to IRD ioctl and waits for the kernel done interruption
 * 
 * @param  vargp IRD File Descriptor
 * 
 * @return       IRD File Descriptor
 * 
 */
void *Interrupt_Thread_Function(void *vargp);


/**
 * @brief Frees allocated memory for kernel handler
 * @param _s_handle Kernel handling pointer
 */
void SAF_free_Kernel(SAF_Kernel_Handle_T _s_handle) {

  /* unmap kernel's virtual memory */
  if (_s_handle->p_mapped_addr) {
    munmap(_s_handle->p_mapped_addr, _s_handle->s_kernel_config.offset);
  }

  /* Free Kernel's config structure data */
  Free_Kernel_Config(&_s_handle->s_kernel_config);

  /* Free Kernel's signals structures */
  free(_s_handle->as_kernel_signals);

  /* Free Kernel's structure */
  free(_s_handle);
}


/**
 * @brief Atexit()'s callback. Frees allocated memory following a linked list logic.
 */
void SAF_Safe_Exit(void) {

  SAF_Kernel_Handle_T node;
  SAF_Kernel_Handle_T next;

  int ret = 0;
  int fd = 0;
  SAF_Buffer_T last;
  SAF_Buffer_T prev;

  beExit = SAF_TRUE;

  close(ird_ctx.ird_fd);

  node = vg_kernel_handle_instance;

  /* TODO: On second iteration, kernel object is already free
  causing double free error. This shouldn't happen when there are more 
  than 1 kernel. 
  temporary solution: only free the first kernel instance. */
  
  //do {
    next = node->next;
    SAF_free_Kernel(node);
  //} while(next);

  if (vg_p_c_config_json!=NULL) {
    free(((char *)vg_p_c_config_json));
  }

  /* Free buffer allocs */
  fd = open("/dev/"MAD_DEV_FILENAME, 0);
  if (fd < 0) {
    DEBUG_ERR("Cannot open device file: %s\n", MAD_DEV_FILENAME);
  }

  if (vg_buffer_handle_instance) {
    last = vg_buffer_handle_instance;
    while(last->next) {
      prev = last;
      last = last->next;
      
      DEBUG_LOG_2("\nMAD_IOCTL_FREE\n");
      DEBUG_LOG_2("mad.phyaddr: 0x%lX\n", prev->mo.phyaddr);
      DEBUG_LOG_2("mad.virt_to_phyaddr: 0x%lX\n", prev->mo.virt_to_phyaddr);
      DEBUG_LOG_2("mad.virtaddr: 0x%p\n", prev->mo.virtaddr);

      ret = ioctl(fd, MAD_IOCTL_FREE, &prev->mo);

      if ( 0 > ret )
      {
        DEBUG_ERR("ioctl MAD_IOCTL_FREE failed: %d\n", ret);
      }

      free(prev);
    }
  }
  close(fd);
}



/**
 * @brief Gets SAF Config file.
 * 
 * @return  SAF_TRUE on success.
 *          SAF_FALSE otherwise.
 */
SAF_BOOL SAF_init(void) {
  SAF_BOOL b_status;

#if (SAF_DEBUG > 0)
  SAF_print_ver();
#endif
  DEBUG_LOG_1("Debugging enabled level %d\n", (int)SAF_DEBUG );

  beExit = SAF_FALSE;

  /* Get Kernel Json Config */
  b_status = Json_To_Buffer(KERNEL_CONFIG_FILE, ((char **)&vg_p_c_config_json));

#ifdef USE_IRQ
  sem_init(&queue, 0, 0);
  
  ird_ctx.ird_fd = open("/dev/"IRD_DEV_FILENAME, O_RDWR | O_SYNC, 0);

  if (ird_ctx.ird_fd < 0) {
    DEBUG_ERR("Cannot open device file: %s\n", IRD_DEV_FILENAME);
    return SAF_FALSE;
  }
#endif

  atexit(SAF_Safe_Exit);
  signal(SIGTERM, SAF_Safe_Exit);

  return b_status;
}

/**
 * @brief Search for kernel config metadata and allocates control
 *        structure.
 *  
 * @param[in]  _pc_kernel_id Kernel ID string.
 *
 * @return        SAF_Kernel_Handle on success.
 *                NULL pointer otherwise.
 */
SAF_Kernel_Handle_T SAF_getKernel(const char *_p_c_kernel_id) {

  SAF_Kernel_Handle_T s_handle;

  SAF_BOOL b_status;
  uint32_t fd = 0;

  /* Eval for param and config file loaded */
  if (!_p_c_kernel_id || !vg_p_c_config_json)
    { return NULL; }


  /* Allocate handle instance */
  s_handle = (SAF_Kernel_Handle_T)calloc(1, sizeof(struct SAF_Kernel_Handle_Typedef));

  /* Eval allocation */
  if (!s_handle) { 
    free(((char *)vg_p_c_config_json));
    return NULL;
  }

  /* Set Kernel status */
  s_handle->e_status = E_SAF_UNCONFIGURED;

  /* Parse JSON file */
  b_status = Parse_JSON_Config_File(vg_p_c_config_json,
                                    _p_c_kernel_id,
                                    &(s_handle->s_kernel_config));

  /* Eval Kernel Config Parse */
  if (b_status != SAF_TRUE) {
    /* Free kernel config */
    SAF_free_Kernel(s_handle);
    return NULL;
  }

  /* Get kernel's signals size */
  s_handle->signals_size = s_handle->s_kernel_config.signals_size;

  /* Allocate kernel's signal reference structures */
  s_handle->as_kernel_signals = (SAF_Kernel_Signals_T *)calloc(s_handle->signals_size, sizeof(SAF_Kernel_Signals_T));


  if (!s_handle->s_kernel_config.signals) {
    /* Free kernel config */
    SAF_free_Kernel(s_handle);
    return NULL;
  }

  /* TODO: mmap util */
  fd = open("/dev/mem", O_RDWR|O_SYNC); 
  s_handle->p_mapped_addr = (uint32_t *)mmap(NULL,  s_handle->s_kernel_config.offset, PROT_READ|PROT_WRITE, MAP_SHARED, fd, s_handle->s_kernel_config.base_addr);

  if (s_handle->p_mapped_addr == MAP_FAILED) {
    /* Free kernel config */
    DEBUG_ERR("map failed. Please run with sudo.\n");
    SAF_free_Kernel(s_handle);
    return NULL;
  }

  /* Check ready bit */
  //TODO => Check this part
  //I disable that for now because it seems that conv kerne lcan be rerun without being IDLE using the RESTART bit.
  //if (SAF_getKernelSatus(s_handle) != E_SAF_IDLE){
  //  /* Free kernel config */
  //  SAF_free_Kernel(s_handle);
  //  return NULL;
  //}

  /* Reset */
  do
  {
    *((uint32_t *)(s_handle->p_mapped_addr + 0x07f8)) = 0xf;
    sleep(1);
    DEBUG_LOG_1("SAF_runKernel Reset state 0x%x\n", *((uint32_t *)s_handle->p_mapped_addr));
    /* TODO: Timeout */
  }while ( (*((uint32_t *)s_handle->p_mapped_addr) != 0x4) && (*((uint32_t *)s_handle->p_mapped_addr) != 0x6) );

  DEBUG_LOG_1("SAF_runKernel Reset state 0x%x\n", *((uint32_t *)s_handle->p_mapped_addr));


  /* Store Instance in local linked list last node */
  if (vg_kernel_handle_instance) {
    SAF_Kernel_Handle_T last;

    last = vg_kernel_handle_instance;

    while(last->next) {
      last = last->next;
    }

    last->next = s_handle;

  } else {
    vg_kernel_handle_instance = s_handle;
  }

  /* Increment kernel instance counter */
  vg_kernel_handle_instances++;

  /* Set Kernel init flag */
  s_handle->b_init = SAF_TRUE;

  /* Return kernel handle */
  return s_handle;
}


/**
 * @brief Allocates buffer of size _s_bytes of physical memory
 * 
 * @param[in]  _s_bytes Min size of memery to allocate
 * 
 * @return          SAF_Buffer_T handle on Success.
 *                  NULL pointer otherwise.
 */
SAF_Buffer_T SAF_bufferAlloc(size_t _s_bytes) {
  SAF_Buffer_T s_buffer;
  int fd = 0;
  int8_t ret = 0;

  s_buffer = (SAF_Buffer_T)calloc(1, sizeof(struct SAF_Buffer_Typedef));

  if (s_buffer == NULL) {
    DEBUG_ERR("Buffer allocation failed\n");
    return NULL;
  }

  fd = open("/dev/"MAD_DEV_FILENAME, O_RDWR | O_SYNC, 0);

  if (fd < 0) {
    DEBUG_ERR("Cannot open device file: %s\n", MAD_DEV_FILENAME);
    free(s_buffer);
    return NULL;
  }

  s_buffer->mo.size = Align_For_Page_Size(_s_bytes);

  ret = ioctl (fd, MAD_IOCTL_MALLOC, &s_buffer->mo);

  if (ret < 0) {
    DEBUG_ERR("ioctl MAD_IOCTL_MALLOC failed: %d\n", ret);
    free(s_buffer);
    close(fd);
    return NULL;
  }

  DEBUG_LOG_2("\nMAD_IOCTL_MALLOC\n");
  DEBUG_LOG_2("mad.phyaddr: 0x%lX\n", s_buffer->mo.phyaddr);
  DEBUG_LOG_2("mad.size: 0x%lX\n", s_buffer->mo.size);
  DEBUG_LOG_2("mad.virt_to_phyaddr: 0x%lX\n", s_buffer->mo.virt_to_phyaddr);
  DEBUG_LOG_2("mad.virtaddr: 0x%p\n", s_buffer->mo.virtaddr);

  s_buffer->user_virtaddr = mmap(NULL,
                                 s_buffer->mo.size,
                                 PROT_READ | PROT_WRITE,
                                 MAP_SHARED,
                                 fd,
                                 s_buffer->mo.virt_to_phyaddr);

  if (s_buffer->user_virtaddr==MAP_FAILED) {
    DEBUG_ERR("mad mmap failed\n");
    DEBUG_ERR("Value of errno: %d\n", errno);
    DEBUG_ERR("Error opening file: %s\n", strerror(errno));
    ioctl (fd, MAD_IOCTL_FREE, &s_buffer->mo);
    close(fd);
    free(s_buffer);
    return NULL;
  }

  close(fd);

  if (vg_buffer_handle_instance) {
    SAF_Buffer_T last;

    last = vg_buffer_handle_instance;

    while(last->next) {
      last = last->next;
    }

    last->next = s_buffer;

  } else {
     vg_buffer_handle_instance = s_buffer;
  }

  /* NOTE: Check final member name */
  return s_buffer;
}


/**
 * @brief Frees physical memory of SAF_Buffer_T
 * 
 * @param[in] _s_buffer Buffer handle
 */
void SAF_bufferFree(SAF_Buffer_T _s_buffer) {

  int fd = 0;
  int8_t ret = 0;

  fd = open("/dev/"MAD_DEV_FILENAME, 0);

  if (fd < 0) {
    DEBUG_ERR("Cannot open device file: %s\n", MAD_DEV_FILENAME);
    return;
  }

  ret = ioctl (fd, MAD_IOCTL_FREE, &_s_buffer->mo);

  if (ret < 0) {
    DEBUG_ERR("ioctl MAD_IOCTL_FREE failed: %d\n", ret);
    return;
  }

  /* TODO: Check */
  /* mmap physical|virtual allocated memmory */
  ret = munmap(_s_buffer->user_virtaddr,
                                  _s_buffer->mo.size);

  if (ret != 0) {
    DEBUG_ERR("mad mmap failed\n");
    ioctl (fd, MAD_IOCTL_FREE, &_s_buffer->mo);
    close(fd);
    free(_s_buffer);
    return;
  }

  close(fd);

}


/**
 * @brief Adds value to buffer in defined memory index
 * 
 * @param[in]  _s_buff    Buffer handle
 * @param[in]  _u64_index Index to store value
 * @param[in]  _u32_value  32bit value to be stored
 * 
 * @return  SAF_TRUE on success.
 *          SAF_FALSE otherwise.
 */
SAF_BOOL SAF_bufferAddItem(SAF_Buffer_T _s_buff, uint64_t _u64_index, uint32_t _u32_value) {
  /* Eval params */
  if (!_s_buff)
    { return SAF_FALSE; }

  /* Allocated memory index max = _s_buff->mo.size - 1 */
  if (_u64_index >= _s_buff->mo.size)
    { return SAF_FALSE; }

  /* Assign value */
  ((uint32_t *)_s_buff->user_virtaddr)[_u64_index] = _u32_value;

  /* DEBUG */
  //printf("_s_buff->user_virtaddr)[%u] -> 0x%X\n", _u64_index, &(((uint32_t *)_s_buff->user_virtaddr)[_u64_index]));
  DEBUG_LOG_3( "AddItem@phy->0x%X: in->0x%X - added->0x%lX\n", (((uint32_t *)_s_buff->mo.phyaddr )+ _u64_index), _u32_value, ((uint32_t *)_s_buff->user_virtaddr)[_u64_index] );


  /* NOTE: Check value assigned */
  if (((uint32_t *)_s_buff->user_virtaddr)[_u64_index] != _u32_value)
    { return SAF_FALSE; }

  return SAF_TRUE;
}

/**
 * @brief Gets item on define memory index from buffer
 * 
 * @param[in]  _s_buff    Buffer handle
 * @param[in]  _u64_index Index of item
 * 
 * @return  SAF_TRUE on success.
 *          SAF_FALSE otherwise.
 */
SAF_BOOL SAF_bufferGetItem(SAF_Buffer_T _s_buff, uint64_t _u64_index, uint32_t * _p_u32_value) {
  /* Eval params */
  if (!_s_buff)
    { return SAF_FALSE; }

  /* Allocated memory index max = _s_buff->mo.size - 1 */
  if (_u64_index >= _s_buff->mo.size) /* TODO: mo.size is bytes, compared with element index (may cause ovfl) */
    { return SAF_FALSE; }

  DEBUG_LOG_3( "GetItem@phy->0x%X virt-value->0x%X\n", (((uint32_t *)_s_buff->mo.phyaddr )+ _u64_index), ((uint32_t *)_s_buff->user_virtaddr)[_u64_index]);
   
  *_p_u32_value = ((uint32_t *)_s_buff->user_virtaddr)[_u64_index];

  return SAF_TRUE;
}


/**
 * @brief      Reads _u32_size bytes from _u32_offset of _p_data pointer
 *
 * @param[in]  _s_buff      Buffer handle
 * @param[in]  _u32_offset  Offset of bytes from buffer start addres
 * @param[in]  _u32_size    Amount of bytes to read from buffer
 * @param      _p_data      Pointer of memory to store readed data
 *
 * @return  SAF_TRUE on success.
 *          SAF_FALSE otherwise.
 */
SAF_BOOL SAF_bufferRead(SAF_Buffer_T _s_buff, uint32_t _u32_offset, uint32_t _u32_size, void* _p_data) {

  void *p_buff;
  uint64_t u64_p_buff;

  /* Eval params */
  DEBUG_LOG_2( "_s_buff->user_virtaddr: 0x%lX\n", _s_buff->user_virtaddr);
  DEBUG_LOG_2( "_s_buff->mo.phyaddr: 0x%lX\n", _s_buff->mo.phyaddr);
  DEBUG_LOG_2( "_s_buff->mo.size: 0x%lX\n", _s_buff->mo.size);
  DEBUG_LOG_2( "_u32_offset: %d\n", _u32_offset);
  DEBUG_LOG_2( "_u32_size: %d\n", _u32_size);
  DEBUG_LOG_2( "_p_data: 0x%lX\n", _p_data);

  if (!_s_buff || !_p_data || _u32_size == 0)
    { return SAF_FALSE; }

  /* Allocated memory index max = _s_buff->mo.size - 1 */
  if ((_u32_offset + _u32_size) > _s_buff->mo.size)
    { return SAF_FALSE; }

  u64_p_buff = _s_buff->user_virtaddr;
  u64_p_buff += _u32_offset;

  p_buff = u64_p_buff; /* TODO: Eval correct address byte increment */

  DEBUG_LOG_2("u64_p_buff: 0x%lX - 0x%lX\n", u64_p_buff, u64_p_buff + _u32_size);
  DEBUG_LOG_2("p_buff: 0x%lX - 0x%lX\n", p_buff, u64_p_buff + _u32_size);

  memcpy(_p_data, p_buff, _u32_size); /* NOTE: No return value is reserved to indicate error */

    return SAF_TRUE;
}



/**
 * @brief      Writes _u32_size bytes from _p_data pointer to _u32_offset bytes of Buffer start address
 *
 * @param[in]  _s_buff      Buffer handle
 * @param[in]  _u32_offset  Offset of bytes from buffer start addres
 * @param[in]  _u32_size    Amount of bytes to write to buffer
 * @param      _p_data      Pointer of memory to read data from
 *
 * @return  SAF_TRUE on success.
 *          SAF_FALSE otherwise.
 */
SAF_BOOL SAF_bufferWrite(SAF_Buffer_T _s_buff, uint32_t _u32_offset, uint32_t _u32_size, void* _p_data) {

  void *p_buff;
  uint64_t u64_p_buff;

  /* Eval params */
  DEBUG_LOG_2( "_s_buff->user_virtaddr: 0x%lX\n", _s_buff->user_virtaddr);
  DEBUG_LOG_2( "_s_buff->mo.phyaddr: 0x%lX\n", _s_buff->mo.phyaddr);
  DEBUG_LOG_2( "_s_buff->mo.size: 0x%lX\n", _s_buff->mo.size);
  DEBUG_LOG_2( "_u32_offset: %d\n", _u32_offset);
  DEBUG_LOG_2( "_u32_size: %d\n", _u32_size);
  DEBUG_LOG_2( "_p_data: 0x%lX\n", _p_data);

  if (!_s_buff || !_p_data || _u32_size == 0)
    { return SAF_FALSE; }

  /* Allocated memory index max = _s_buff->mo.size - 1 */
  if ((_u32_offset + _u32_size) > _s_buff->mo.size)
    { return SAF_FALSE; }

  u64_p_buff = _s_buff->user_virtaddr;
  u64_p_buff += _u32_offset;

  p_buff = u64_p_buff; /* TODO: Eval correct address byte increment */

  DEBUG_LOG_2("u64_p_buff: 0x%lX - 0x%lX\n", u64_p_buff, u64_p_buff + _u32_size);
  DEBUG_LOG_2("p_buff: 0x%lX - 0x%lX\n", p_buff, u64_p_buff + _u32_size);

  memcpy(p_buff, _p_data, _u32_size); /* NOTE: No return value is reserved to indicate error */

  return SAF_TRUE;
}



/**
 * [SAF_bufferGetPhyAddr description]
 * @param  _s_buff    [description]
 * @param  _u64_index [description]
 * @return            [description]
 */
uint64_t SAF_bufferGetPhyAddr(SAF_Buffer_T _s_buff, uint64_t _u64_index) {

  /* Eval params */
  if (!_s_buff)
    { return 0; }


  /* Allocated memory index max = _s_buff->mo.size - 1 */
  if (_u64_index >= _s_buff->mo.size)
    { return 0; }


  return (uint64_t)((uint64_t)_s_buff->mo.phyaddr + _u64_index);
}




/**
 * [SAF_bufferGetVirtAddr description]
 * @param  _s_buff    [description]
 * @param  _u64_index [description]
 * @return            [description]
 */
uint64_t SAF_bufferGetVirtAddr(SAF_Buffer_T _s_buff, uint64_t _u64_index) {
  /* Eval params */
  if (!_s_buff)
    { return 0; }


  /* Allocated memory index max = _s_buff->mo.size - 1 */
  if (_u64_index >= _s_buff->mo.size)
    { return 0; }


  return (uint64_t)(_s_buff->mo.virtaddr + _u64_index);
}


/**
 * @brief      { function_description }
 *
 * @param[in]  _s_buff  The s buffer
 *
 * @return     { description_of_the_return_value }
 */
uint32_t SAF_bufferGetSize(SAF_Buffer_T _s_buff) {
  /* Eval params */
  if (!_s_buff)
    { return 0; }

  return (uint32_t)(_s_buff->mo.size);
}



/**
 * @brief Calls to IRD ioctl and waits for the kernel done interruption
 * 
 * @param  vargp IRD File Descriptor
 * 
 * @return       IRD File Descriptor
 * 
 */
void *Interrupt_Thread_Function(void *vargp){
  int8_t ret = 0;
  ird_context_t * ctx;

  ctx = (ird_context_t *)vargp; 

  ret = ioctl (ctx->ird_fd, IRD_IOCTL_WAIT_INTERRUPTION, &io);
  if (ret < 0) {
    DEBUG_ERR("ioctl IRD_IOCTL_WAIT_INTERRUPTION failed: %d\n", ret);
  } else {
     sem_post(&queue);
  }

  /* TODO: Disable kernel IRQ's from ctx->s_handle (kernel handle) */
  
  // *((uint64_t *)(kernel_base_ptr + GLOBAL_IRQ_ENABLE_OFFSET)) = *((uint64_t *)(kernel_base_ptr + GLOBAL_IRQ_ENABLE_OFFSET)) | 0x0;
  // *((uint64_t *)(kernel_base_ptr + IP_IRQ_ENABLE_OFFSET)) = *((uint64_t *)(kernel_base_ptr + IP_IRQ_ENABLE_OFFSET)) | 0x0;
  // *((uint64_t *)(kernel_base_ptr + IP_IRQ_STATUS_OFFSET)) = *((uint64_t *)(kernel_base_ptr + IP_IRQ_STATUS_OFFSET)) | 0x0;

  return vargp;
}


/**
 * @brief Polls kernel status info from control register.
 * 
 * @param[in]  _s_handle Kernel handler structure.
 * 
 * @return           SAF_Kernel_Status_Typedef status.
 */
SAF_Kernel_Status_Typedef SAF_getKernelSatus(SAF_Kernel_Handle_T _s_handle) {

  uint32_t u32_kernel_status;

  /* Eval arguments */
  if (!_s_handle)
    { return E_SAF_ERROR; }


  /* Eval for inited Kernel */
  /*
    if (!_s_handle->b_init)
    { return E_SAF_ERROR; }
  */

  /* Get kernel control register value */
  u32_kernel_status = *((uint32_t *)(_s_handle->p_mapped_addr + _s_handle->s_kernel_config.control_reg.offset));

  /* If busy poll for donen and release  */
  if (_s_handle->e_status == E_SAF_BUSY) {
    if (((u32_kernel_status >> (_s_handle->s_kernel_config.control_reg.bit_pos.ap_done-1)&1)) == 1){
      /* Set Kernel status */
      _s_handle->e_status = E_SAF_DONE;
    }
  } else {
    if (((u32_kernel_status >> (_s_handle->s_kernel_config.control_reg.bit_pos.ap_idle-1)&1)) == 1){
      /* Set Kernel status to ready if unconfig or idle */
      _s_handle->e_status = E_SAF_IDLE;
    } else {
      /* ERROR: ready bit is not set */
      _s_handle->e_status = E_SAF_ERROR;
    }
  }

  return _s_handle->e_status;
}


/**
 * @brief Dumps argument data to kernel registers and sets the required flags to fire the kernel.
 * @param[in]  _s_handle       Kernel handler structure pointer.
 * @param[in]  _u32_timeout_us Timeout of kernel run status polling.
 * 
 * @return                 SAF_TRUE on Success.
 *                         SAF_FALSE otherwise.
 */
SAF_BOOL SAF_runKernel(SAF_Kernel_Handle_T _s_handle, SAF_UINT32 _u32_timeout_us) {

  SAF_BOOL b_status = SAF_FALSE;
  uint8_t i_sig;
  uint32_t u32_mask_value;
  uint32_t u32_timeout_us = 0;

  /* Eval arguments */
  if (!_s_handle)
    { return SAF_FALSE; }

  /* Eval for inited Kernel */
  if (!_s_handle->b_init)
    { return SAF_FALSE; }

  /* Check kernel is not busy 
    NOTE: Mirar si esto funciona bien
  */
  if ((_s_handle->e_status) == E_SAF_BUSY)
    { return SAF_FALSE; }

  /* Set busy status */
  _s_handle->e_status = E_SAF_BUSY;

  /* Run interruption thread */
  if (_u32_timeout_us <= 0){
    //pthread_create(&interruption_thread, NULL, Interrupt_Thread_Function, &ird_ctx);

    //*((uint32_t *)(_s_handle->p_mapped_addr + GLOBAL_IRQ_ENABLE_OFFSET)) = *((uint32_t *)(_s_handle->p_mapped_addr + GLOBAL_IRQ_ENABLE_OFFSET)) | 0x1;
    //*((uint32_t *)(_s_handle->p_mapped_addr + IP_IRQ_ENABLE_OFFSET)) = *((uint32_t *)(_s_handle->p_mapped_addr + IP_IRQ_ENABLE_OFFSET)) | 0x1;
    DEBUG_LOG_1("Timeout cannot be <= 0!\n");
    assert(_u32_timeout_us > 0);
  }

  /* Dump argument data to kernel registers */
  for (i_sig = 0 ; i_sig < _s_handle->s_kernel_config.signals_size ; i_sig++) {

    if (_s_handle->s_kernel_config.signals[i_sig].size_bits == 64) {
      u32_mask_value = 0xFFFFFFFF;
    } else {
      /* Create mask for value */
      u32_mask_value = 0 | (0xFFFFFFFF >> (32-_s_handle->s_kernel_config.signals[i_sig].size_bits));
    }

    /* Assign value in register defined bits */
    *((uint32_t *)(_s_handle->p_mapped_addr + _s_handle->s_kernel_config.signals[i_sig].offset)) = 
      ((_s_handle->as_kernel_signals[i_sig].signal_value & u32_mask_value) << _s_handle->s_kernel_config.signals[i_sig].pos);
 

    DEBUG_LOG_3( "Signal[%d] %s\n", i_sig, _s_handle->s_kernel_config.signals[i_sig].name );
    DEBUG_LOG_3( "Signal offset 0x%X\n", _s_handle->s_kernel_config.signals[i_sig].offset );
    DEBUG_LOG_3( "u32mask: 0x%X\n", u32_mask_value );
    DEBUG_LOG_3( "_s_handle->as_kernel_signals[%u].signal_value: 0x%X\n", i_sig, _s_handle->as_kernel_signals[i_sig].signal_value );
    DEBUG_LOG_3( "s_handle->p_mapped_addr + _s_handle->s_kernel_config.signals[%u].offset: 0x%X\n", i_sig, _s_handle->p_mapped_addr + _s_handle->s_kernel_config.signals[i_sig].offset );
    DEBUG_LOG_3( "Value: 0x%X\n", *((uint32_t *)(_s_handle->p_mapped_addr + _s_handle->s_kernel_config.signals[i_sig].offset)) );


    //SAF_Debug_Physical_Mem(_s_handle->s_kernel_config.base_addr, _s_handle->s_kernel_config.signals[i_sig].offset);


      /* NOTE: Quick fix for 64bit addressing */
      if (_s_handle->s_kernel_config.signals[i_sig].size_bits == 64) {

        *((uint32_t *)(_s_handle->p_mapped_addr + _s_handle->s_kernel_config.signals[i_sig].offset + 4)) = 0;

        DEBUG_LOG_2( "s_handle->p_mapped_addr + _s_handle->s_kernel_config.signals[i_sig].offset: 0x%X\n", _s_handle->p_mapped_addr + _s_handle->s_kernel_config.signals[i_sig].offset + 4 );
        DEBUG_LOG_2( "Value: %lu\n", *((uint32_t *)(_s_handle->p_mapped_addr + _s_handle->s_kernel_config.signals[i_sig].offset + 4)) );

      }

  }

  DEBUG_LOG_1("SAF_runKernel polling\n");

  /* Set restart bit */
  //*((uint32_t *)_s_handle->p_mapped_addr) = 0x80;
  //DEBUG_LOG_1("SAF_runKernel Autorestart set state 0x%x\n", *((uint32_t *)_s_handle->p_mapped_addr));

  /* Set start bit on kernel's control register  */
  *((uint32_t *)_s_handle->p_mapped_addr) = *((uint32_t *)_s_handle->p_mapped_addr) | 
  (1 << (_s_handle->s_kernel_config.control_reg.bit_pos.ap_start-1));

  //DEBUG_LOG_1("SAF_runKernel start set state 0x%x\n", *((uint32_t *)_s_handle->p_mapped_addr));
  DEBUG_LOG_1("SAF_runKernel start set\n");
  //SAF_Debug_Kernel(_s_handle);

  uint32_t timer = 0;

  /* If timeout is set poll run kernel in polling mode */
  if (_u32_timeout_us > 0) {
    u32_timeout_us = 0; 
    while(SAF_getKernelSatus(_s_handle) != E_SAF_DONE){
    
      //usleep(1);
      //u32_timeout_us++;

      //if (u32_timeout_us%100000==0) {
      //  timer++;
      //  printf("SAF_runKernel %ums\n", 100*timer);
      //}

      //if (_u32_timeout_us == u32_timeout_us) {
      //  printf("SAF_runKernel Timed out\n");
      //  return b_status;
      //}
    }
    DEBUG_LOG_1("SAF_runKernel End polling state 0x%x\n", *((uint32_t *)_s_handle->p_mapped_addr));

    /* Set continue to high */
    //*((uint32_t *)_s_handle->p_mapped_addr) = *((uint32_t *)_s_handle->p_mapped_addr) | 
    //(1 << (_s_handle->s_kernel_config.control_reg.bit_pos.ap_continue-1));


    /* Wait until the accelerator is done again and enters DONE and IDLE */
    while ( *((uint32_t *)_s_handle->p_mapped_addr) != 0x6)
    {
      /* TODO: Timeout */
    }

    /* Reset */
    *((uint32_t *)(_s_handle->p_mapped_addr + 0x07f8)) = 0xf;
    DEBUG_LOG_1("SAF_runKernel Reset state 0x%x\n", *((uint32_t *)_s_handle->p_mapped_addr));

  } else {
    sem_wait(&queue);
    *((uint32_t *)(_s_handle->p_mapped_addr + GLOBAL_IRQ_ENABLE_OFFSET)) = *((uint32_t *)(_s_handle->p_mapped_addr + GLOBAL_IRQ_ENABLE_OFFSET)) | 0x0;
    *((uint32_t *)(_s_handle->p_mapped_addr + IP_IRQ_ENABLE_OFFSET)) = *((uint32_t *)(_s_handle->p_mapped_addr + IP_IRQ_ENABLE_OFFSET)) | 0x0;
    //*((uint32_t *)(_s_handle->p_mapped_addr + IP_IRQ_STATUS_OFFSET)) = *((uint32_t *)(_s_handle->p_mapped_addr + IP_IRQ_STATUS_OFFSET)) | 0x0;
    //uint32_t read_interrupt = *((uint32_t *)(_s_handle->p_mapped_addr + IP_IRQ_STATUS_OFFSET));
    
    DEBUG_LOG_1("Interruptions shutdown.\n");

    if (io.status == 0){
      DEBUG_LOG_1("Kernel Interruption timeout. Condition evaluated to false.\n");
      return b_status;
    } else if (io.status == 1){
      DEBUG_LOG_1("Kernel interruption timeout. Condition evaluated to true.\n");
      return b_status;
    } else if (io.status > 1){
      DEBUG_LOG_1("Kernel interruption received! Elapsed time %d ms\n", (io.status/io.myHZ*1000));
    }
  }

  b_status = SAF_TRUE;

  return b_status;
}


/**
 * @brief Configures arguments on kernel
 *
 * @param[in]  _s_handle    Kernel handler structure pointer.
 * @param[in]  _pc_arg_name Name of the argument to set.
 * @param[in]  _u32_value   Value of the argument to set.
 * 
 * @return                  SAF_TRUE on Success.
 *                          SAF_FALSE otherwise.
 */
SAF_BOOL SAF_setArg(SAF_Kernel_Handle_T _s_handle, char *_pc_arg_name, void * void_ptr) {

  SAF_BOOL b_status = SAF_FALSE;
  uint8_t i_sig;
  // uint8_t i_shift_count, i_shift_bits;

  /* Eval arguments */
  if (!_s_handle || !_pc_arg_name || !void_ptr)
    { return SAF_FALSE; }

  /* Eval for inited Kernel */
  if (!_s_handle->b_init)
    { return SAF_FALSE; }

  /* Check kernel is not busy 
    NOTE: Mirar si esto funciona bien
  */
  if ((_s_handle->e_status) == E_SAF_BUSY)
    { return SAF_FALSE; }

  /* TODO: Pensar que más podemos validar como error a este punto */

// #define REG_SIZE_BITS (32U)

  /* Eval for valid argument on config file */
  for (i_sig = 0 ; i_sig < _s_handle->signals_size ; i_sig++) {
    if (strcmp(_pc_arg_name, _s_handle->s_kernel_config.signals[i_sig].name) == 0) {
      b_status = SAF_TRUE;

      /* Calc num of 32bit buff addr shifts */
      // i_shift_count = _s_handle->s_kernel_config.signals[i_sig].size_bits % REG_SIZE_BITS;
      /* TODO: Eval for 0 */

      // i_shift_bits = 0;

      /* Set arg on handle structure */
      _s_handle->as_kernel_signals[i_sig].signal_index = i_sig;

      if ( _s_handle->s_kernel_config.signals[i_sig].type == E_KERNEL_SIGNAL_BUFFER) 
      {
        /* TODO: Fix to expect a Buffer_TypeDef Param */
        _s_handle->as_kernel_signals[i_sig].signal_value = ((SAF_Buffer_T)void_ptr)->mo.phyaddr;
      }
      else if (_s_handle->s_kernel_config.signals[i_sig].type == E_KERNEL_SIGNAL_VALUE) 
      {
        _s_handle->as_kernel_signals[i_sig].signal_value = (*(uint32_t*)void_ptr);
      }
      else
      {
        return SAF_FALSE;
      }

      DEBUG_LOG_1( "Signal %s [%d]: 0x%X\n", _s_handle->s_kernel_config.signals[i_sig].name, i_sig, _s_handle->as_kernel_signals[i_sig].signal_value );

      break;
    }
  }

  return b_status;
}


/**
 * @brief Configures arguments on kernel
 *
 * @param[in]  _s_handle    Kernel handler structure pointer.
 * @param[in]  _u32_index   Index of the argument to set.
 * @param[in]  _u32_value   Value of the argument to set.
 * 
 * @return                  SAF_TRUEon Success.
 *                          SAF_FALSE otherwise.
 */
SAF_BOOL SAF_setArgByIndex(SAF_Kernel_Handle_T _s_handle, uint32_t _u32_index, void * void_ptr) {

  uint8_t i_sig;
  // uint8_t i_shift_count, i_shift_bits;

  /* Eval arguments */
  if (!_s_handle || !void_ptr)
    { return SAF_FALSE; }

  /* Eval for inited Kernel */
  if (!_s_handle->b_init)
    { return SAF_FALSE; }

  /* Eval for signal index overflow */
  if (_u32_index >= _s_handle->s_kernel_config.signals_size)
    { return SAF_FALSE; }

  /* Check kernel is not busy 
    NOTE: Mirar si esto funciona bien
  */
  if ((_s_handle->e_status) == E_SAF_BUSY)
    { return SAF_FALSE; }

  /* Set arg on handle structure */
  _s_handle->as_kernel_signals[_u32_index].signal_index = _u32_index;

  if ( _s_handle->s_kernel_config.signals[_u32_index].type == E_KERNEL_SIGNAL_BUFFER) 
  {
    /* TODO: Fix to expect a Buffer_TypeDef Param */
    _s_handle->as_kernel_signals[_u32_index].signal_value = ((SAF_Buffer_T)void_ptr)->mo.phyaddr;
  }
  else if (_s_handle->s_kernel_config.signals[_u32_index].type == E_KERNEL_SIGNAL_VALUE) 
  {
    _s_handle->as_kernel_signals[_u32_index].signal_value = (*(uint32_t*)void_ptr);
  }
  else
  {
    return SAF_FALSE;
  }

  DEBUG_LOG_2( "Signal %s [%d]: 0x%X\n", _s_handle->s_kernel_config.signals[_u32_index].name, _u32_index, _s_handle->as_kernel_signals[_u32_index].signal_value );

  return SAF_TRUE;
}


/**
 * @brief      Returns signal name by its index  
 *
 * @param[in]  _s_handle     Kernel handler structure pointer.
 * @param[in]  _u8_arg_idx   Index of the signal.
 * @param[out] _pc_arg_name  Char pointer to store signal name. 
 *
 * @return                  SAF_TRUE on Success.
 *                          SAF_FALSE otherwise.
 */
SAF_BOOL SAF_getArgNameByIndex(SAF_Kernel_Handle_T _s_handle, uint8_t _u8_arg_idx, char *_pc_arg_name) {

  /* Eval arguments */
  if (!_s_handle || !_pc_arg_name)
    { return SAF_FALSE; }

  /* Eval for inited Kernel */
  if (!_s_handle->b_init)
    { return SAF_FALSE; }

  /* Check kernel is not busy */
  if ((_s_handle->e_status) == E_SAF_BUSY)
    { return SAF_FALSE; }


  /* Check for invalid signal index */
  if (_u8_arg_idx >= _s_handle->s_kernel_config.signals_size)
    { return SAF_FALSE; }


  strcpy(_pc_arg_name, _s_handle->s_kernel_config.signals[_u8_arg_idx].name);

  return SAF_TRUE;
}

/**
 * @cond TEST_DOCS
 */

/**
 * @brief Function to test address manipulation and shifting operations.
 * 
 * @param [in] _s_handle Kernel handler structure.
 * 
 * @return           TRUE if successful.
 *                   SAF_FALSE otherwise.
 */
SAF_BOOL SAF_Debug_Offsets_Shifts(SAF_Kernel_Handle_T _s_handle) {

  uint8_t i_sig = 0;
  uint32_t u32_mask_value;

  DEBUG_LOG_2("mmaped base address: 0x%08lx\n\r\n\r", ((uint64_t)_s_handle->p_mapped_addr));

  for (i_sig = 0; i_sig < _s_handle->s_kernel_config.signals_size; i_sig ++) {
    DEBUG_LOG_2("Signal %s address: 0x%08lx\n", _s_handle->s_kernel_config.signals[i_sig].name,
      (uint64_t)(_s_handle->p_mapped_addr + _s_handle->s_kernel_config.signals[i_sig].offset));

    u32_mask_value = 0 | (0xFFFFFFFF >> (32 -_s_handle->s_kernel_config.signals[i_sig].size_bits));

    // printf("_s_handle->as_kernel_signals[i_sig].signal_value: 0x%08X\n\r", _s_handle->as_kernel_signals[i_sig].signal_value);
    // printf("s_handle->s_kernel_config.signals[i_sig].size_bits: %d\n\r", _s_handle->s_kernel_config.signals[i_sig].size_bits);
    // printf("_s_handle->s_kernel_config.signals[i_sig].pos: %d\n\r", _s_handle->s_kernel_config.signals[i_sig].pos);

    // printf("value_shift: %d\n\r", 32 -_s_handle->s_kernel_config.signals[i_sig].size_bits);
    // printf("u32_mask_value: 0x%08X\n\r", u32_mask_value);

    *(uint32_t *)(_s_handle->p_mapped_addr + _s_handle->s_kernel_config.signals[i_sig].offset) |= ((_s_handle->as_kernel_signals[i_sig].signal_value & u32_mask_value) << _s_handle->s_kernel_config.signals[i_sig].pos);

    DEBUG_LOG_2("Signal %s value: 0x%08x\n\r\n\r", _s_handle->s_kernel_config.signals[i_sig].name,
      *(uint32_t *)(_s_handle->p_mapped_addr + _s_handle->s_kernel_config.signals[i_sig].offset));
  }

  return SAF_TRUE;


}


/**
 * [SAF_Debug_Physical_Mem description]
 * @param  _p_baseAddr [description]
 * @param  _offset     [description]
 * @return             [description]
 */
SAF_BOOL SAF_Debug_Physical_Mem(void * _p_baseAddr, uint16_t _offset) {

  int des_p[2];
  int pid;


  if (!_p_baseAddr)
    { return SAF_FALSE; }

  DEBUG_LOG_1("Base Addr: 0x%08X\n", _p_baseAddr);
  DEBUG_LOG_1("Offset: 0x%08X\n", _offset);

  if(pipe(des_p) == -1) {
    perror("Pipe failed\n");
    return SAF_FALSE;
  }

  pid = fork();

  if(pid == 0)            //first fork
  {
    close(STDOUT_FILENO);  //closing stdout
    dup(des_p[1]);         //replacing stdout with pipe write 
    close(des_p[0]);       //closing pipe read
    close(des_p[1]);

    const char* prog1[3] = { "devmem2", 0, 0};
    char ac_arg_buff[200];

    /* Assign phyaddr string to args param */
    sprintf(ac_arg_buff, "0x%lX", ((uint64_t)(_p_baseAddr + _offset)));

    prog1[1] = ac_arg_buff; 
    //fprintf(stderr, "THIS %s\n", (char *)prog1[1]);

    DEBUG_LOG_1("execvp(%s, %s %s %s);\n", prog1[0], prog1[0], prog1[1], prog1[2]);

    execvp(prog1[0], ((char * const*)prog1));

    perror("execvp of devmem2 failed\n");

    return SAF_FALSE;

  } else if(pid > 0){

    //DEBUG_LOG_1(printf("Parent Process\nChild Process PID: %d\n", pid));
    char ac_stdout_buff[100];

    close(des_p[1]);

    int nbytes;

    while((nbytes = read(des_p[0], ac_stdout_buff, sizeof(ac_stdout_buff))) != 0) {
      nbytes = read(des_p[0], ac_stdout_buff, sizeof(ac_stdout_buff));

      //DEBUG_LOG_1("bytes readed: %d\n", nbytes);
      DEBUG_LOG_1("devmem2 output: %.*s\n", nbytes-1, ac_stdout_buff);
    }

    wait(0);

  } else {

    DEBUG_ERR("fork failed!!\n");

  }

  return SAF_TRUE;
}

/**
 * @brief Prints physical memory contents of mad allocated buffer
 * 
 * @param[in]  _s_buff    mad driver allocated memory handle
 * @param[in]  _u64_index Item index to print
 * 
 * @return  SAF_TRUE on Success.
 *          SAF_FALSE otherwise.
 */
SAF_BOOL SAF_Debug_Buffer_Physical_Mem(SAF_Buffer_T _s_buff, uint64_t _u64_index) {

  int des_p[2];

  if (!_s_buff)
    { return SAF_FALSE; }

  if(pipe(des_p) == -1) {
    perror("Pipe failed\n");
    return SAF_FALSE;
  }

  if(fork() == 0)            //first fork
  {
    close(STDOUT_FILENO);  //closing stdout
    dup(des_p[1]);         //replacing stdout with pipe write 
    close(des_p[0]);       //closing pipe read
    close(des_p[1]);

    const char* prog1[3] = { "devmem2", 0, 0};
    // const char* prog1[3] = { "devmem2", 0, 0};
    char ac_arg_buff[100];

    /* Assign phyaddr string to args param */
    sprintf(ac_arg_buff, "0x%X", ((uint32_t)(_s_buff->mo.phyaddr + (_u64_index * 4))));

    prog1[1] = ac_arg_buff;

    fprintf(stderr, "\nexecvp(%s, %s %s %s);\n", prog1[0], prog1[0], prog1[1], prog1[2]);

    execvp(prog1[0], ((char * const*)prog1));
    perror("execvp of ls failed\n");
    return SAF_FALSE;
  } else {

    char ac_stdout_buff[100];

    close(des_p[1]);

    int nbytes = read(des_p[0], ac_stdout_buff, sizeof(ac_stdout_buff));
    DEBUG_LOG_1("0x%X: (%.*s)\n", ((uint32_t)(_s_buff->mo.phyaddr + (_u64_index * 4))), nbytes-1, ac_stdout_buff);
    wait(0);

  }

  return SAF_TRUE;

}

/**
 * [SAF_Debug_Kernel description]
 * @param  _s_handle [description]
 * @return             [description]
 */
SAF_BOOL SAF_Debug_Kernel(SAF_Kernel_Handle_T _s_handle) {
  DEBUG_LOG_1("*******************************\n");
  DEBUG_LOG_1("Status of the kernel %s:\n", _s_handle->s_kernel_config.name);
  DEBUG_LOG_1("CONTROL_REGISTER: 0x%x\n", *((uint32_t *)_s_handle->p_mapped_addr));
  DEBUG_LOG_1("GLOBAL_IRQ_ENABLE: 0x%x\n", *((uint32_t *)(_s_handle->p_mapped_addr + GLOBAL_IRQ_ENABLE_OFFSET)));
  DEBUG_LOG_1("IP_IRQ_ENABLE: 0x%x\n", *((uint32_t *)(_s_handle->p_mapped_addr + IP_IRQ_ENABLE_OFFSET)));
  DEBUG_LOG_1("*******************************\n");

  return SAF_TRUE;
}

/**
 * [SAF_dump_to_u32_ptr  description]
 * @param  _value [description]
 * @return        [description]
 */
uint32_t SAF_dump_to_u32_ptr (float _f_value){
  uint32_t u32_tmp_value = 0;
  memcpy(&u32_tmp_value, &_f_value, sizeof(u32_tmp_value));
  return u32_tmp_value;
}

/**
 * [SAF_get_from_u32_ptr  description]
 * @param  _u32_value [description]
 * @return            [description]
 */
float SAF_get_from_u32_ptr (uint32_t _u32_value){
  float f_tmp_value = 0.0;

  DEBUG_LOG_3( "SAF_get_from_u32_ptr _u32_value: %i-%X\n", _u32_value, _u32_value );

  memcpy(&f_tmp_value, &_u32_value, sizeof(f_tmp_value));

  DEBUG_LOG_3( "SAF_get_from_u32_ptr f_tmp_value: %6.4f\n", f_tmp_value );

  return f_tmp_value;
}

/**
 * @brief Print the library version
 * 
 * @param[in] 
 * 
 * @return
 * 
 */
void SAF_print_ver(void)
{
  DEBUG_WARN("****************\n");
  DEBUG_WARN("SAF Version: %s\n", COMMIT);
  DEBUG_WARN("SAF compiled on: %s %s\n", __DATE__, __TIME__);
  DEBUG_WARN("****************\n");
}

/*
  TODO:
  
 * in SAF_Kernel_Signals_T
     + Change uint32_t signal_value to void * signal_value

 

 * in SAF_Kernel_Handle_T SAF_getKernel(const char *_p_c_kernel_id)
     + Add dynamic memory allocation of signal_value data size based on signal size (for each signal)

 

 * in SAF_setArg(SAF_Kernel_Handle_T _s_handle, char *_pc_arg_name, void * void_ptr)
     + Add signal buffer value set based on signal size in bits (ptr_data (0:31), ptr_data(32:63), etc)

 

 * in SAF_runKernel(SAF_Kernel_Handle_T _s_handle, SAF_UINT32 _u32_timeout_us)
     + Change signal value mask calculation
     + Add calculation of signal_value chunks (32 bit number of registers to set)
     + Add signal_value offset registers to set
 */

/**
 * @endcond
 */

/** @} (end addtogroup SAF) *************************************/
